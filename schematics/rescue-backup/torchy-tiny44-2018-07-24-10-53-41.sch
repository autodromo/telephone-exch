EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mikee
LIBS:mikes
LIBS:torchy-tiny44-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Audio Visual Controller with Tiny44"
Date "2017-10-16"
Rev "02"
Comp "Millstream Computing"
Comment1 "(C) 2017"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR?
U 1 1 57E53B41
P 1450 2600
F 0 "#PWR?" H 1450 2350 50  0001 C CNN
F 1 "GND" H 1450 2450 50  0000 C CNN
F 2 "" H 1450 2600 50  0000 C CNN
F 3 "" H 1450 2600 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 2600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 57E53B86
P 3850 1350
F 0 "#PWR?" H 3850 1100 50  0001 C CNN
F 1 "GND" H 3850 1200 50  0000 C CNN
F 2 "" H 3850 1350 50  0000 C CNN
F 3 "" H 3850 1350 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3850 1350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 57E8F433
P 10200 5150
F 0 "#PWR?" H 10200 4900 50  0001 C CNN
F 1 "GND" H 10200 5000 50  0000 C CNN
F 2 "" H 10200 5150 50  0000 C CNN
F 3 "" H 10200 5150 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    10200 5150
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 57EE230F
P 8450 650
F 0 "#PWR?" H 8450 500 50  0001 C CNN
F 1 "+5V" H 8450 790 50  0000 C CNN
F 2 "" H 8450 650 50  0000 C CNN
F 3 "" H 8450 650 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8450 650 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 57EE46EB
P 5750 1100
F 0 "#PWR?" H 5750 950 50  0001 C CNN
F 1 "+5V" H 5750 1240 50  0000 C CNN
F 2 "" H 5750 1100 50  0000 C CNN
F 3 "" H 5750 1100 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5750 1100
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 57EE4720
P 1450 1150
F 0 "#PWR?" H 1450 1000 50  0001 C CNN
F 1 "+5V" H 1450 1290 50  0000 C CNN
F 2 "" H 1450 1150 50  0000 C CNN
F 3 "" H 1450 1150 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 1150
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R1
U 1 1 57F79B33
P 5550 4000
F 0 "R1" V 5630 4000 50  0000 C CNN
F 1 "1K" V 5550 4000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5480 4000 50  0001 C CNN
F 3 "" H 5550 4000 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5550 4000
	0    1    -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 57F79B36
P 8400 5300
F 0 "#PWR?" H 8400 5050 50  0001 C CNN
F 1 "GND" H 8400 5150 50  0000 C CNN
F 2 "" H 8400 5300 50  0000 C CNN
F 3 "" H 8400 5300 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8400 5300
	-1   0    0    -1  
$EndComp
$Comp
L BC546 Q1
U 1 1 57FA0D1F
P 4250 5900
F 0 "Q1" H 4450 5975 50  0000 L CNN
F 1 "BC547B" H 4450 5900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Molded_Wide" H 4450 5825 50  0001 L CIN
F 3 "" H 4250 5900 50  0000 L CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4250 5900
	-1   0    0    -1  
$EndComp
$Comp
L DIODE D1
U 1 1 57FA0F8B
P 3450 5250
F 0 "D1" H 3450 5400 50  0000 C CNN
F 1 "1N4001" H 3450 5075 50  0000 C CNN
F 2 "Discret:D3" H 3450 5250 50  0001 C CNN
F 3 "" H 3450 5250 50  0000 C CNN
F 4 "Rapid" H 3450 5250 60  0001 C CNN "Supplier"
F 5 "£" H 3450 5250 60  0001 C CNN "Cost"
F 6 "supp" H 0   0   60  0001 C CNN "supplier"
F 7 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 8 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 9 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3450 5250
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 57FA15B4
P 4150 6250
F 0 "#PWR?" H 4150 6000 50  0001 C CNN
F 1 "GND" H 4150 6100 50  0000 C CNN
F 2 "" H 4150 6250 50  0000 C CNN
F 3 "" H 4150 6250 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4150 6250
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R2
U 1 1 57FA16C2
P 4550 2400
F 0 "R2" V 4630 2400 50  0000 C CNN
F 1 "1K" V 4550 2400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4480 2400 50  0001 C CNN
F 3 "" H 4550 2400 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4550 2400
	0    -1   1    0   
$EndComp
$Comp
L +5V #PWR?
U 1 1 57FA1E61
P 3450 4700
F 0 "#PWR?" H 3450 4550 50  0001 C CNN
F 1 "+5V" H 3450 4840 50  0000 C CNN
F 2 "" H 3450 4700 50  0000 C CNN
F 3 "" H 3450 4700 50  0000 C CNN
F 4 "supp" H 0   250 60  0001 C CNN "supplier"
F 5 "supp#" H 0   250 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   250 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   250 60  0001 C CNN "manf#"
	1    3450 4700
	-1   0    0    -1  
$EndComp
$Comp
L AVR-ISP-6 ISP1
U 1 1 57FA4C9D
P 3400 1000
F 0 "ISP1" H 3295 1240 50  0000 C CNN
F 1 "ISP" H 3135 770 50  0000 L BNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" V 2880 1040 50  0001 C CNN
F 3 "" H 3375 1000 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3400 1000
	-1   0    0    -1  
$EndComp
$Comp
L Screw_Terminal_1x03 J1
U 1 1 58063FC8
P 1350 5150
F 0 "J1" H 1350 5500 50  0000 C TNN
F 1 "CTB5201/BK3" V 1200 5150 50  0000 C TNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MKDS1.5-3pol" H 1350 4825 50  0001 C CNN
F 3 "" H 1325 5250 50  0001 C CNN
F 4 "Rapid" H 1350 5150 60  0001 C CNN "Supplier"
F 5 "£" H 1350 5150 60  0001 C CNN "Cost"
	1    1350 5150
	1    0    0    1   
$EndComp
$Comp
L DFPlayer_Mini-RESCUE-torchy U2
U 1 1 580F4ABF
P 7600 4850
F 0 "U2" H 7250 4300 50  0000 C CNN
F 1 "DFPlayer_Mini" V 7550 4750 50  0000 C CNN
F 2 "Oddities:DFPlayer_Mini" H 7500 4800 50  0001 C CNN
F 3 "" H 7500 4800 50  0000 C CNN
F 4 "£" H 7600 4850 60  0001 C CNN "Cost"
	1    7600 4850
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 580F53E1
P 8400 4350
F 0 "#PWR?" H 8400 4200 50  0001 C CNN
F 1 "+5V" H 8400 4490 50  0000 C CNN
F 2 "" H 8400 4350 50  0000 C CNN
F 3 "" H 8400 4350 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8400 4350
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R5
U 1 1 580F5923
P 8450 4950
F 0 "R5" V 8500 4800 50  0000 C CNN
F 1 "1K" V 8450 4950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8380 4950 50  0001 C CNN
F 3 "" H 8450 4950 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8450 4950
	0    -1   1    0   
$EndComp
$Comp
L R-RESCUE-torchy R4
U 1 1 580F59CC
P 8000 3400
F 0 "R4" V 7950 3250 50  0000 C CNN
F 1 "10R" V 8000 3400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7930 3400 50  0001 C CNN
F 3 "" H 8000 3400 50  0000 C CNN
F 4 "supp" H -450 -1450 60  0001 C CNN "supplier"
F 5 "supp#" H -450 -1450 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -450 -1450 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -450 -1450 60  0001 C CNN "manf#"
	1    8000 3400
	0    -1   1    0   
$EndComp
$Comp
L R-RESCUE-torchy R8
U 1 1 5821FCB7
P 5550 4550
F 0 "R8" V 5630 4550 50  0000 C CNN
F 1 "1K" V 5550 4550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5480 4550 50  0001 C CNN
F 3 "" H 5550 4550 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5550 4550
	0    1    -1   0   
$EndComp
$Comp
L C_Small C1
U 1 1 582DA90F
P 1450 2050
F 0 "C1" H 1460 2120 50  0000 L CNN
F 1 "100nF" H 1460 1970 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.4mm_W2.1mm_P2.50mm" H 1450 2050 50  0001 C CNN
F 3 "" H 1450 2050 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 2050
	1    0    0    -1  
$EndComp
$Comp
L VTX-214-010-212 U1
U 1 1 58965069
P 1750 7200
F 0 "U1" H 2150 7050 60  0000 C CNN
F 1 "VTX-214-010-212" H 1800 7550 60  0000 C CNN
F 2 "Mikes_Stuff:VTX-214-010-212" H 1850 6950 60  0001 C CNN
F 3 "" H 1850 6950 60  0000 C CNN
F 4 "84-2168" H 1750 7200 60  0001 C CNN "Supp ID"
F 5 "Rapid" H 0   0   60  0001 C CNN "Supplier"
F 6 "£6.82" H 1750 7200 60  0001 C CNN "Cost"
	1    1750 7200
	1    0    0    -1  
$EndComp
$Comp
L Screw_Terminal_1x03 J2
U 1 1 5896538A
P 750 7150
F 0 "J2" H 750 7500 50  0000 C TNN
F 1 "CTB5201/BK3" V 600 7150 50  0000 C TNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MKDS1.5-3pol" H 750 6825 50  0001 C CNN
F 3 "" H 725 7250 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    750  7150
	1    0    0    1   
$EndComp
$Comp
L FUSE_Small F1
U 1 1 58965591
P 1200 6950
F 0 "F1" H 1160 7010 50  0000 L CNN
F 1 "FUSE_Small" H 1080 6890 50  0000 L CNN
F 2 "Fuse_Holders_and_Fuses:Fuse_TE5_Littlefuse-395Series" H 1200 6950 50  0001 C CNN
F 3 "" H 1200 6950 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1200 6950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58965804
P 2650 7450
F 0 "#PWR?" H 2650 7200 50  0001 C CNN
F 1 "GND" H 2650 7300 50  0000 C CNN
F 2 "" H 2650 7450 50  0000 C CNN
F 3 "" H 2650 7450 50  0000 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    2650 7450
	-1   0    0    -1  
$EndComp
$Comp
L +12V #PWR?
U 1 1 589658F4
P 2950 6850
F 0 "#PWR?" H 2950 6700 50  0001 C CNN
F 1 "+12V" H 2950 6990 50  0000 C CNN
F 2 "" H 2950 6850 50  0000 C CNN
F 3 "" H 2950 6850 50  0000 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    2950 6850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 58965CDD
P 3900 6800
F 0 "#PWR?" H 3900 6650 50  0001 C CNN
F 1 "+5V" H 3900 6940 50  0000 C CNN
F 2 "" H 3900 6800 50  0000 C CNN
F 3 "" H 3900 6800 50  0000 C CNN
F 4 "supp" H 300 400 60  0001 C CNN "supplier"
F 5 "supp#" H 300 400 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 400 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 400 60  0001 C CNN "manf#"
	1    3900 6800
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R9
U 1 1 58966796
P 4200 7400
F 0 "R9" V 4280 7400 50  0000 C CNN
F 1 "1K" V 4200 7400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4130 7400 50  0001 C CNN
F 3 "" H 4200 7400 50  0000 C CNN
F 4 "supp" H 950 0   60  0001 C CNN "supplier"
F 5 "supp#" H 950 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 950 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 950 0   60  0001 C CNN "manf#"
	1    4200 7400
	0    -1   1    0   
$EndComp
$Comp
L LED_ALT D2
U 1 1 5896681A
P 4450 7250
F 0 "D2" H 4450 7350 50  0000 C CNN
F 1 "LED_ALT" H 4450 7150 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 4450 7250 50  0001 C CNN
F 3 "" H 4450 7250 50  0001 C CNN
F 4 "supp" H 850 0   60  0001 C CNN "supplier"
F 5 "supp#" H 850 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 850 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 850 0   60  0001 C CNN "manf#"
	1    4450 7250
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR?
U 1 1 589683A2
P 3100 800
F 0 "#PWR?" H 3100 650 50  0001 C CNN
F 1 "+5V" H 3100 940 50  0000 C CNN
F 2 "" H 3100 800 50  0000 C CNN
F 3 "" H 3100 800 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3100 800 
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58968497
P 3050 1200
F 0 "#PWR?" H 3050 950 50  0001 C CNN
F 1 "GND" H 3050 1050 50  0000 C CNN
F 2 "" H 3050 1200 50  0000 C CNN
F 3 "" H 3050 1200 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3050 1200
	-1   0    0    -1  
$EndComp
$Comp
L LM7805CT U3
U 1 1 5896FBB7
P 3450 7000
F 0 "U3" H 3250 7200 50  0000 C CNN
F 1 "LM7805CT" H 3450 7200 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 3450 7100 50  0001 C CIN
F 3 "" H 3450 7000 50  0001 C CNN
F 4 "Rapid" H 300 0   60  0001 C CNN "supplier"
F 5 "47-3313" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£0.327; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    3450 7000
	1    0    0    -1  
$EndComp
$Comp
L TL2020-v1.0 U5
U 1 1 5896FC78
P 9600 4900
F 0 "U5" H 9250 4750 60  0000 C CNN
F 1 "TL2020-v1.0" H 9500 5650 60  0000 C CNN
F 2 "Mikes_Stuff:YL2020-v1.0" H 10200 5100 60  0001 C CNN
F 3 "" H 10200 5100 60  0000 C CNN
F 4 "£" H 9600 4900 60  0001 C CNN "Cost"
	1    9600 4900
	1    0    0    -1  
$EndComp
$Comp
L G5Q-1 RL1
U 1 1 589737BD
P 3100 5450
F 0 "RL1" H 3750 5800 50  0000 L CNN
F 1 "G5Q-1" H 3750 5700 50  0000 L CNN
F 2 "Relays_THT:Relay_SANYOU_SRD_Series_Form_C" H 3750 5600 50  0001 L CNN
F 3 "" H 3300 5250 50  0001 C CNN
F 4 "60-4661" H 3100 5450 60  0001 C CNN "Supp ID"
F 5 "Rapid" H 3100 5450 60  0001 C CNN "Supplier"
F 6 "£" H 3100 5450 60  0001 C CNN "Cost"
	1    3100 5450
	-1   0    0    -1  
$EndComp
$Comp
L LED_ALT D3
U 1 1 58974449
P 3900 5450
F 0 "D3" H 3900 5550 50  0000 C CNN
F 1 "LED_ALT" H 3900 5350 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 3900 5450 50  0001 C CNN
F 3 "" H 3900 5450 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3900 5450
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-torchy R10
U 1 1 589744E1
P 3900 5050
F 0 "R10" V 3980 5050 50  0000 C CNN
F 1 "470R" V 3900 5050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3830 5050 50  0001 C CNN
F 3 "" H 3900 5050 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3900 5050
	-1   0    0    -1  
$EndComp
NoConn ~ 10000 4450
NoConn ~ 10000 4550
$Comp
L Screw_Terminal_1x04 J4
U 1 1 5897585A
P 9600 3500
F 0 "J4" H 9600 3950 50  0000 C TNN
F 1 "Screw_Terminal_1x04" V 9450 3500 50  0000 C TNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MKDS1.5-4pol" H 9600 3075 50  0001 C CNN
F 3 "" H 9575 3700 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    9600 3500
	-1   0    0    1   
$EndComp
$Comp
L TEST GND1
U 1 1 589A26DC
P 1000 2200
F 0 "GND1" H 1000 2500 50  0000 C BNN
F 1 "TEST" H 1000 2450 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 1000 2200 50  0001 C CNN
F 3 "" H 1000 2200 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1000 2200
	1    0    0    -1  
$EndComp
$Comp
L TEST TP2
U 1 1 589A2A2F
P 5200 1400
F 0 "TP2" H 5200 1700 50  0000 C BNN
F 1 "TEST" H 5200 1650 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 5200 1400 50  0001 C CNN
F 3 "" H 5200 1400 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5200 1400
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 589A5D32
P 2950 7100
F 0 "C2" H 2975 7200 50  0000 L CNN
F 1 "100uF" H 2975 7000 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D7.5mm_P2.50mm" H 2988 6950 50  0001 C CNN
F 3 "" H 2950 7100 50  0001 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    2950 7100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 J3
U 1 1 58B1B3D4
P 6000 1500
F 0 "J3" H 6000 1700 50  0000 C CNN
F 1 "CONN_01X03" V 6100 1500 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 6000 1500 50  0001 C CNN
F 3 "" H 6000 1500 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    6000 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58B1B46F
P 5450 2350
F 0 "#PWR?" H 5450 2100 50  0001 C CNN
F 1 "GND" H 5450 2200 50  0000 C CNN
F 2 "" H 5450 2350 50  0000 C CNN
F 3 "" H 5450 2350 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5450 2350
	-1   0    0    -1  
$EndComp
Text Notes 6200 1550 0    50   ~ 0
OFF-BOARD \nSENSORS
$Comp
L Jumper_NO_Small JP2
U 1 1 593BDA86
P 8500 3400
F 0 "JP2" H 8500 3480 50  0000 C CNN
F 1 "PWR" H 8510 3340 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8500 3400 50  0001 C CNN
F 3 "" H 8500 3400 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8500 3400
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR?
U 1 1 593BDBF7
P 7750 3350
F 0 "#PWR?" H 7750 3200 50  0001 C CNN
F 1 "+12V" H 7750 3490 50  0000 C CNN
F 2 "" H 7750 3350 50  0000 C CNN
F 3 "" H 7750 3350 50  0000 C CNN
F 4 "supp" H -400 -50 60  0001 C CNN "supplier"
F 5 "supp#" H -400 -50 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -400 -50 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -400 -50 60  0001 C CNN "manf#"
	1    7750 3350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 594E7491
P 8350 2800
F 0 "#PWR?" H 8350 2550 50  0001 C CNN
F 1 "GND" H 8350 2650 50  0000 C CNN
F 2 "" H 8350 2800 50  0000 C CNN
F 3 "" H 8350 2800 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8350 2800
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 594E7530
P 8350 1500
F 0 "#PWR?" H 8350 1350 50  0001 C CNN
F 1 "+5V" H 8350 1640 50  0000 C CNN
F 2 "" H 8350 1500 50  0000 C CNN
F 3 "" H 8350 1500 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8350 1500
	-1   0    0    -1  
$EndComp
NoConn ~ 8350 2200
$Comp
L CONN_01X05 J5
U 1 1 594EC30D
P 8550 2100
F 0 "J5" H 8550 2400 50  0000 C CNN
F 1 "DS3231 RTC" V 8650 2100 50  0000 C CNN
F 2 "Mike:DS3231" H 8550 2100 50  0001 C CNN
F 3 "" H 8550 2100 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8550 2100
	1    0    0    -1  
$EndComp
$Comp
L ATTINY44-20PU U4
U 1 1 59662196
P 2750 2100
F 0 "U4" H 1900 2850 50  0000 C CNN
F 1 "ATTINY44-20PU" H 3450 1350 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm" H 2750 1900 50  0001 C CIN
F 3 "" H 2750 2100 50  0001 C CNN
F 4 "73-5118" H 2750 2100 60  0001 C CNN "Supp ID"
F 5 "Rapid" H 2750 2100 60  0001 C CNN "Supplier"
F 6 "£2.11" H 0   0   60  0001 C CNN "Cost"
	1    2750 2100
	1    0    0    -1  
$EndComp
$Comp
L POT_TRIM RV1
U 1 1 596734F6
P 5200 2350
F 0 "RV1" V 5025 2350 50  0000 C CNN
F 1 "POT_TRIM" V 5100 2350 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 5200 2350 50  0001 C CNN
F 3 "" H 5200 2350 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5200 2350
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR?
U 1 1 596737D3
P 4950 2350
F 0 "#PWR?" H 4950 2200 50  0001 C CNN
F 1 "+5V" H 4950 2490 50  0000 C CNN
F 2 "" H 4950 2350 50  0000 C CNN
F 3 "" H 4950 2350 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4950 2350
	-1   0    0    -1  
$EndComp
$Comp
L POT_TRIM RV2
U 1 1 5967415E
P 4100 1350
F 0 "RV2" V 3925 1350 50  0000 C CNN
F 1 "POT_TRIM" V 4000 1350 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 4100 1350 50  0001 C CNN
F 3 "" H 4100 1350 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4100 1350
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR?
U 1 1 5967D5B8
P 4300 1350
F 0 "#PWR?" H 4300 1200 50  0001 C CNN
F 1 "+5V" H 4300 1490 50  0000 C CNN
F 2 "" H 4300 1350 50  0000 C CNN
F 3 "" H 4300 1350 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4300 1350
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 596C9ED4
P 7050 1150
F 0 "#PWR?" H 7050 1000 50  0001 C CNN
F 1 "+5V" H 7050 1290 50  0000 C CNN
F 2 "" H 7050 1150 50  0000 C CNN
F 3 "" H 7050 1150 50  0000 C CNN
F 4 "supp" H 1300 50  60  0001 C CNN "supplier"
F 5 "supp#" H 1300 50  60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1300 50  60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1300 50  60  0001 C CNN "manf#"
	1    7050 1150
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R6
U 1 1 596C9F96
P 7300 1550
F 0 "R6" V 7380 1550 50  0000 C CNN
F 1 "4K7" V 7300 1550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7230 1550 50  0001 C CNN
F 3 "" H 7300 1550 50  0000 C CNN
F 4 "Rapid" H 7300 1550 60  0001 C CNN "Supplier"
F 5 "£" H 7300 1550 60  0001 C CNN "Cost"
F 6 "supp" H 2750 -850 60  0001 C CNN "supplier"
	1    7300 1550
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R3
U 1 1 596CA0C2
P 7100 1550
F 0 "R3" V 7180 1550 50  0000 C CNN
F 1 "4K7" V 7100 1550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7030 1550 50  0001 C CNN
F 3 "" H 7100 1550 50  0000 C CNN
F 4 "supp" H 2550 -850 60  0001 C CNN "supplier"
F 5 "supp#" H 2550 -850 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 2550 -850 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 2550 -850 60  0001 C CNN "manf#"
	1    7100 1550
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X04 J6
U 1 1 596DE49C
P 6200 2750
F 0 "J6" H 6200 3000 50  0000 C CNN
F 1 "CONN_01X04" V 6300 2750 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-04_04x2.54mm_Straight" H 6200 2750 50  0001 C CNN
F 3 "" H 6200 2750 50  0001 C CNN
	1    6200 2750
	1    0    0    1   
$EndComp
$Comp
L +5V #PWR?
U 1 1 596DE56B
P 6000 2450
F 0 "#PWR?" H 6000 2300 50  0001 C CNN
F 1 "+5V" H 6000 2590 50  0000 C CNN
F 2 "" H 6000 2450 50  0000 C CNN
F 3 "" H 6000 2450 50  0000 C CNN
F 4 "supp" H 1050 100 60  0001 C CNN "supplier"
F 5 "supp#" H 1050 100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1050 100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1050 100 60  0001 C CNN "manf#"
	1    6000 2450
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 596DE5F0
P 6000 3050
F 0 "#PWR?" H 6000 2800 50  0001 C CNN
F 1 "GND" H 6000 2900 50  0000 C CNN
F 2 "" H 6000 3050 50  0000 C CNN
F 3 "" H 6000 3050 50  0000 C CNN
F 4 "supp" H 550 700 60  0001 C CNN "supplier"
F 5 "supp#" H 550 700 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 550 700 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 550 700 60  0001 C CNN "manf#"
	1    6000 3050
	-1   0    0    -1  
$EndComp
Text Notes 6400 2850 0    60   ~ 0
SPARE. CAN BE USED FOR \nULTRASONIC ETC.
Wire Wire Line
	1450 1150 1450 1950
Wire Wire Line
	1450 1800 1650 1800
Wire Wire Line
	1000 2300 1650 2300
Wire Wire Line
	4150 6100 4150 6250
Wire Wire Line
	4800 2400 4700 2400
Wire Wire Line
	3450 5450 3450 5700
Wire Wire Line
	3100 5700 4150 5700
Wire Wire Line
	3100 5700 3100 5550
Connection ~ 3450 5700
Wire Wire Line
	8400 4350 8400 4550
Wire Wire Line
	8400 4550 8100 4550
Wire Wire Line
	8400 5300 8400 5150
Wire Wire Line
	8100 5150 8750 5150
Wire Wire Line
	8100 4850 8300 4850
Wire Wire Line
	8300 4950 8100 4950
Wire Wire Line
	8100 4650 8650 4650
Wire Wire Line
	8650 4650 8650 4000
Wire Wire Line
	8650 4000 5700 4000
Wire Wire Line
	5700 4550 7150 4550
Wire Wire Line
	4850 4550 5400 4550
Wire Wire Line
	5400 4000 4850 4000
Connection ~ 4850 4000
Connection ~ 1450 1800
Wire Wire Line
	1450 2150 1450 2600
Wire Wire Line
	2200 7250 3450 7250
Connection ~ 2650 7250
Wire Wire Line
	3850 6950 4450 6950
Wire Wire Line
	3900 6950 3900 6800
Connection ~ 1450 2300
Connection ~ 3900 6950
Wire Wire Line
	2650 7400 4050 7400
Connection ~ 2650 7400
Wire Wire Line
	4400 2150 4400 1100
Wire Wire Line
	4400 1100 3550 1100
Wire Wire Line
	4600 900  4600 2000
Wire Wire Line
	4600 900  3550 900 
Wire Wire Line
	4500 1000 4500 1900
Wire Wire Line
	4500 1000 3550 1000
Wire Wire Line
	4700 600  2900 600 
Wire Wire Line
	2900 600  2900 1000
Wire Wire Line
	2900 1000 3300 1000
Wire Wire Line
	3300 900  3100 900 
Wire Wire Line
	3100 900  3100 800 
Wire Wire Line
	3300 1100 3050 1100
Wire Wire Line
	3050 1100 3050 1200
Wire Wire Line
	8600 4950 9200 4950
Wire Wire Line
	9200 4850 8600 4850
Wire Wire Line
	10000 4950 10200 4950
Wire Wire Line
	10200 4850 10200 5150
Wire Wire Line
	10000 4850 10200 4850
Connection ~ 10200 4950
Wire Wire Line
	9200 4450 9100 4450
Wire Wire Line
	950  6950 1100 6950
Wire Wire Line
	1300 6950 1400 6950
Wire Wire Line
	1400 7350 1400 7050
Wire Wire Line
	950  7350 1400 7350
Connection ~ 1050 6950
Wire Wire Line
	1550 5350 1550 6550
Wire Wire Line
	1550 6550 1200 6550
Wire Wire Line
	1200 6550 1200 7350
Connection ~ 1200 7350
Wire Wire Line
	950  7150 1150 7150
Wire Wire Line
	1150 7150 1150 5550
Wire Wire Line
	1150 5550 1650 5550
Wire Wire Line
	1650 5550 1650 5150
Wire Wire Line
	1650 5150 1550 5150
Wire Wire Line
	3100 4950 3100 4850
Wire Wire Line
	3100 4850 3900 4850
Wire Wire Line
	3450 4700 3450 5050
Connection ~ 3450 4850
Wire Wire Line
	4800 2400 4800 5900
Wire Wire Line
	4800 5900 4450 5900
Wire Wire Line
	2700 5550 2700 6700
Wire Wire Line
	2600 4950 2600 4850
Wire Wire Line
	2600 4850 1700 4850
Wire Wire Line
	1700 4850 1700 4950
Wire Wire Line
	1700 4950 1550 4950
Wire Wire Line
	3900 4850 3900 4900
Wire Wire Line
	3900 5200 3900 5300
Wire Wire Line
	3900 5600 3900 5700
Connection ~ 3900 5700
Wire Wire Line
	6900 4550 6900 5700
Wire Wire Line
	6900 5700 10650 5700
Wire Wire Line
	10650 5700 10650 4350
Wire Wire Line
	10650 4350 10000 4350
Connection ~ 6900 4550
Wire Wire Line
	9000 4550 9200 4550
Wire Wire Line
	8900 4650 9200 4650
Wire Wire Line
	8900 3200 8900 4650
Wire Wire Line
	8900 3200 9400 3200
Wire Wire Line
	9200 4350 9200 3800
Wire Wire Line
	9200 3800 9400 3800
Wire Wire Line
	9100 4450 9100 3600
Wire Wire Line
	9100 3600 9400 3600
Wire Wire Line
	9000 4550 9000 3400
Wire Wire Line
	1000 2200 1000 2300
Wire Wire Line
	2200 6950 3050 6950
Connection ~ 2950 6950
Connection ~ 2950 7250
Wire Wire Line
	2950 6950 2950 6850
Wire Wire Line
	5200 1500 5800 1500
Wire Wire Line
	5800 1400 5750 1400
Wire Wire Line
	5750 1400 5750 1100
Wire Wire Line
	5800 1600 5800 1600
Wire Wire Line
	4700 600  4700 2100
Wire Wire Line
	4850 1800 4850 4550
Wire Wire Line
	8350 1500 8350 1900
Wire Wire Line
	8350 2300 8350 2800
Wire Wire Line
	1650 1800 1650 1500
Wire Wire Line
	1650 1500 1700 1500
Wire Wire Line
	1650 2300 1650 2700
Wire Wire Line
	1650 2700 1700 2700
Wire Wire Line
	4600 2000 3800 2000
Wire Wire Line
	3800 1900 8100 1900
Wire Wire Line
	4400 2150 4000 2150
Wire Wire Line
	4000 2150 4000 2700
Connection ~ 4000 2700
Wire Wire Line
	5200 1400 5200 1600
Connection ~ 5200 1500
Wire Wire Line
	3800 2100 7900 2100
Connection ~ 4700 2100
Connection ~ 4500 1900
Wire Wire Line
	3800 1800 4850 1800
Wire Wire Line
	5200 1600 3800 1600
Wire Wire Line
	3800 1700 5800 1700
Wire Wire Line
	5800 1700 5800 1600
Wire Wire Line
	3800 2200 5200 2200
Wire Wire Line
	5350 2350 5450 2350
Wire Wire Line
	3800 2400 4400 2400
Connection ~ 4950 2350
Wire Wire Line
	4950 2350 5050 2350
Wire Wire Line
	3800 1500 4100 1500
Wire Wire Line
	3850 1350 3950 1350
Wire Wire Line
	4250 1350 4300 1350
Wire Wire Line
	7900 2100 7900 2000
Wire Wire Line
	7900 2000 8350 2000
Wire Wire Line
	8100 1900 8100 2100
Wire Wire Line
	8100 2100 8350 2100
Wire Wire Line
	7050 1150 7050 1400
Wire Wire Line
	7050 1400 7300 1400
Connection ~ 7100 1400
Wire Wire Line
	7300 1700 7300 1900
Connection ~ 7300 1900
Wire Wire Line
	7100 1700 7100 2100
Connection ~ 7100 2100
Wire Wire Line
	6000 2600 6000 2450
Wire Wire Line
	6000 2900 6000 3050
Wire Wire Line
	3800 2500 5750 2500
Wire Wire Line
	5750 2500 5750 2700
Wire Wire Line
	5750 2700 6000 2700
Wire Wire Line
	3800 2600 5650 2600
Wire Wire Line
	5650 2600 5650 2800
Wire Wire Line
	5650 2800 6000 2800
NoConn ~ 8100 5050
NoConn ~ 8100 5250
NoConn ~ 7150 5250
NoConn ~ 7150 5050
NoConn ~ 7150 4950
NoConn ~ 7150 4850
NoConn ~ 7150 4750
NoConn ~ 7150 4650
$Comp
L GND #PWR?
U 1 1 596E8C4F
P 7000 5350
F 0 "#PWR?" H 7000 5100 50  0001 C CNN
F 1 "GND" H 7000 5200 50  0000 C CNN
F 2 "" H 7000 5350 50  0000 C CNN
F 3 "" H 7000 5350 50  0000 C CNN
F 4 "supp" H -1400 50  60  0001 C CNN "supplier"
F 5 "supp#" H -1400 50  60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1400 50  60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1400 50  60  0001 C CNN "manf#"
	1    7000 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7150 5150 7000 5150
Wire Wire Line
	7000 5150 7000 5350
Wire Wire Line
	2650 7250 2650 7450
Wire Wire Line
	2200 7050 2200 7250
Connection ~ 2550 7250
$Comp
L Jumper_NO_Small JP1
U 1 1 596F5C8C
P 2550 7050
F 0 "JP1" H 2550 7130 50  0000 C CNN
F 1 "TEST PWR" V 2560 6990 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-02_02x2.54mm_Straight" H 2550 7050 50  0001 C CNN
F 3 "" H 2550 7050 50  0001 C CNN
	1    2550 7050
	0    -1   -1   0   
$EndComp
Connection ~ 2550 6950
Wire Wire Line
	2550 7150 2550 7250
Connection ~ 1350 6950
Wire Wire Line
	1050 6950 1050 6700
Wire Wire Line
	1050 6700 2700 6700
Wire Wire Line
	4000 2700 3800 2700
$Comp
L GND #PWR?
U 1 1 59E4A89A
P 8350 3800
F 0 "#PWR?" H 8350 3550 50  0001 C CNN
F 1 "GND" H 8350 3650 50  0000 C CNN
F 2 "" H 8350 3800 50  0000 C CNN
F 3 "" H 8350 3800 50  0000 C CNN
F 4 "supp" H -50 -1500 60  0001 C CNN "supplier"
F 5 "supp#" H -50 -1500 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -50 -1500 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -50 -1500 60  0001 C CNN "manf#"
	1    8350 3800
	-1   0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 59E4A95C
P 8250 3600
F 0 "C3" H 8275 3700 50  0000 L CNN
F 1 "100uF" H 8275 3500 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D7.5mm_P2.50mm" H 8288 3450 50  0001 C CNN
F 3 "" H 8250 3600 50  0001 C CNN
F 4 "supp" H 5600 -3500 60  0001 C CNN "supplier"
F 5 "supp#" H 5600 -3500 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 5600 -3500 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 5600 -3500 60  0001 C CNN "manf#"
	1    8250 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3350 7750 3400
Wire Wire Line
	7750 3400 7850 3400
Wire Wire Line
	8250 3450 8250 3400
Connection ~ 8250 3400
Wire Wire Line
	8250 3750 8350 3750
Wire Wire Line
	8350 3750 8350 3800
Wire Wire Line
	8150 3400 8400 3400
Wire Wire Line
	8600 3400 9400 3400
Connection ~ 9000 3400
Wire Wire Line
	10000 4750 10200 4750
Wire Wire Line
	10200 4750 10200 3000
Wire Wire Line
	10200 3000 8400 3000
Wire Wire Line
	8400 3000 8400 3400
$Comp
L CP C4
U 1 1 59E4EE18
P 3900 7100
F 0 "C4" H 3925 7200 50  0000 L CNN
F 1 "100uF" H 3925 7000 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D7.5mm_P2.50mm" H 3938 6950 50  0001 C CNN
F 3 "" H 3900 7100 50  0001 C CNN
F 4 "supp" H 1250 0   60  0001 C CNN "supplier"
F 5 "supp#" H 1250 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1250 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1250 0   60  0001 C CNN "manf#"
	1    3900 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 6950 4450 7100
Wire Wire Line
	3900 7400 3900 7250
Connection ~ 3900 7400
Wire Wire Line
	4350 7400 4450 7400
$Comp
L R-RESCUE-torchy R11
U 1 1 59E50593
P 8450 4850
F 0 "R11" V 8500 4700 50  0000 C CNN
F 1 "1K" V 8450 4850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8380 4850 50  0001 C CNN
F 3 "" H 8450 4850 50  0000 C CNN
F 4 "supp" H 0   -100 60  0001 C CNN "supplier"
F 5 "supp#" H 0   -100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   -100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   -100 60  0001 C CNN "manf#"
	1    8450 4850
	0    -1   1    0   
$EndComp
$Comp
L R-RESCUE-torchy R7
U 1 1 59E50637
P 8450 4750
F 0 "R7" V 8500 4600 50  0000 C CNN
F 1 "1K" V 8450 4750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8380 4750 50  0001 C CNN
F 3 "" H 8450 4750 50  0000 C CNN
F 4 "supp" H 0   -200 60  0001 C CNN "supplier"
F 5 "supp#" H 0   -200 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   -200 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   -200 60  0001 C CNN "manf#"
	1    8450 4750
	0    -1   1    0   
$EndComp
Wire Wire Line
	8100 4750 8300 4750
Wire Wire Line
	8600 4750 8750 4750
Wire Wire Line
	8750 4750 8750 5150
Connection ~ 8400 5150
Text Notes 7900 3750 0    60   ~ 0
CHECK THIS\nCAUSES BUZZING
$EndSCHEMATC
