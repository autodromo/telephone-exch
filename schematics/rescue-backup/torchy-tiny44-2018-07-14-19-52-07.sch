EESchema Schematic File Version 2
LIBS:torchy-tiny44-rescue
LIBS:4xxx
LIBS:4xxx_IEEE
LIBS:74xGxx
LIBS:74xx
LIBS:74xx_IEEE
LIBS:Amplifier_Audio
LIBS:Amplifier_Buffer
LIBS:Amplifier_Current
LIBS:Amplifier_Difference
LIBS:Amplifier_Instrumentation
LIBS:Amplifier_Operational
LIBS:Amplifier_Video
LIBS:Analog
LIBS:Analog_ADC
LIBS:Analog_DAC
LIBS:Analog_Switch
LIBS:Audio
LIBS:Battery_Management
LIBS:Comparator
LIBS:Connector
LIBS:Connector_Generic
LIBS:Connector_Generic_MountingPin
LIBS:Connector_Generic_Shielded
LIBS:Converter_ACDC
LIBS:Converter_DCDC
LIBS:CPLD_Altera
LIBS:CPLD_Xilinx
LIBS:CPU
LIBS:CPU_NXP_6800
LIBS:CPU_NXP_68000
LIBS:CPU_PowerPC
LIBS:Device
LIBS:Diode
LIBS:Diode_Bridge
LIBS:Diode_Laser
LIBS:Display_Character
LIBS:Display_Graphic
LIBS:Driver_Display
LIBS:Driver_FET
LIBS:Driver_LED
LIBS:Driver_Motor
LIBS:Driver_Relay
LIBS:DSP_AnalogDevices
LIBS:DSP_Freescale
LIBS:DSP_Microchip_DSPIC33
LIBS:DSP_Motorola
LIBS:DSP_Texas
LIBS:Filter
LIBS:FPGA_Microsemi
LIBS:FPGA_Xilinx
LIBS:FPGA_Xilinx_Artix7
LIBS:FPGA_Xilinx_Kintex7
LIBS:FPGA_Xilinx_Spartan6
LIBS:FPGA_Xilinx_Virtex5
LIBS:FPGA_Xilinx_Virtex6
LIBS:FPGA_Xilinx_Virtex7
LIBS:GPU
LIBS:Graphic
LIBS:Interface
LIBS:Interface_CAN_LIN
LIBS:Interface_CurrentLoop
LIBS:Interface_Ethernet
LIBS:Interface_Expansion
LIBS:Interface_HID
LIBS:Interface_LineDriver
LIBS:Interface_Optical
LIBS:Interface_Telecom
LIBS:Interface_UART
LIBS:Interface_USB
LIBS:Isolator
LIBS:Isolator_Analog
LIBS:Jumper
LIBS:LED
LIBS:Logic_LevelTranslator
LIBS:Logic_Programmable
LIBS:MCU_AnalogDevices
LIBS:MCU_Cypress
LIBS:MCU_Espressif
LIBS:MCU_Intel
LIBS:MCU_Microchip_8051
LIBS:MCU_Microchip_ATmega
LIBS:MCU_Microchip_ATtiny
LIBS:MCU_Microchip_AVR
LIBS:MCU_Microchip_PIC10
LIBS:MCU_Microchip_PIC12
LIBS:MCU_Microchip_PIC16
LIBS:MCU_Microchip_PIC18
LIBS:MCU_Microchip_PIC24
LIBS:MCU_Microchip_PIC32
LIBS:MCU_Microchip_SAME
LIBS:MCU_Microchip_SAML
LIBS:MCU_Module
LIBS:MCU_Nordic
LIBS:MCU_NXP_ColdFire
LIBS:MCU_NXP_HC11
LIBS:MCU_NXP_HC12
LIBS:MCU_NXP_HCS12
LIBS:MCU_NXP_Kinetis
LIBS:MCU_NXP_LPC
LIBS:MCU_NXP_MAC7100
LIBS:MCU_NXP_MCore
LIBS:MCU_NXP_S08
LIBS:MCU_Parallax
LIBS:MCU_SiFive
LIBS:MCU_SiliconLabs
LIBS:MCU_ST_STM8
LIBS:MCU_ST_STM32F0
LIBS:MCU_ST_STM32F1
LIBS:MCU_ST_STM32F2
LIBS:MCU_ST_STM32F3
LIBS:MCU_ST_STM32F4
LIBS:MCU_ST_STM32F7
LIBS:MCU_ST_STM32H7
LIBS:MCU_ST_STM32L0
LIBS:MCU_ST_STM32L1
LIBS:MCU_ST_STM32L4
LIBS:MCU_ST_STM32L4+
LIBS:MCU_Texas
LIBS:MCU_Texas_MSP430
LIBS:Mechanical
LIBS:Memory_Controller
LIBS:Memory_EEPROM
LIBS:Memory_EPROM
LIBS:Memory_Flash
LIBS:Memory_NVRAM
LIBS:Memory_RAM
LIBS:Memory_ROM
LIBS:Memory_UniqueID
LIBS:Mikes
LIBS:Motor
LIBS:Oscillator
LIBS:Potentiometer_Digital
LIBS:power
LIBS:Power_Management
LIBS:Power_Protection
LIBS:Power_Supervisor
LIBS:pspice
LIBS:Reference_Current
LIBS:Reference_Voltage
LIBS:Regulator_Controller
LIBS:Regulator_Current
LIBS:Regulator_Linear
LIBS:Regulator_SwitchedCapacitor
LIBS:Regulator_Switching
LIBS:Relay
LIBS:Relay_SolidState
LIBS:RF
LIBS:RF_AM_FM
LIBS:RF_Amplifier
LIBS:RF_Bluetooth
LIBS:RF_GPS
LIBS:RF_Mixer
LIBS:RF_Module
LIBS:RF_RFID
LIBS:RF_Switch
LIBS:RF_WiFi
LIBS:RF_ZigBee
LIBS:Sensor
LIBS:Sensor_Audio
LIBS:Sensor_Current
LIBS:Sensor_Gas
LIBS:Sensor_Humidity
LIBS:Sensor_Magnetic
LIBS:Sensor_Motion
LIBS:Sensor_Optical
LIBS:Sensor_Pressure
LIBS:Sensor_Proximity
LIBS:Sensor_Temperature
LIBS:Sensor_Touch
LIBS:Sensor_Voltage
LIBS:Switch
LIBS:Timer
LIBS:Timer_PLL
LIBS:Timer_RTC
LIBS:Transformer
LIBS:Transistor_Array
LIBS:Transistor_BJT
LIBS:Transistor_FET
LIBS:Transistor_IGBT
LIBS:Triac_Thyristor
LIBS:Valve
LIBS:Video
LIBS:torchy-tiny44-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Audio Visual Controller with Tiny84"
Date "2018-07-02"
Rev "2"
Comp "Millstream Computing"
Comment1 "(C) 2018"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR?
U 1 1 57E53B41
P 1450 2600
F 0 "#PWR?" H 1450 2350 50  0001 C CNN
F 1 "GND" H 1450 2450 50  0000 C CNN
F 2 "" H 1450 2600 50  0000 C CNN
F 3 "" H 1450 2600 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 2600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 57E53B86
P 4800 1350
F 0 "#PWR?" H 4800 1100 50  0001 C CNN
F 1 "GND" H 4800 1200 50  0000 C CNN
F 2 "" H 4800 1350 50  0000 C CNN
F 3 "" H 4800 1350 50  0000 C CNN
F 4 "supp" H 950 0   60  0001 C CNN "supplier"
F 5 "supp#" H 950 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 950 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 950 0   60  0001 C CNN "manf#"
	1    4800 1350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 57E8F433
P 10650 6000
F 0 "#PWR?" H 10650 5750 50  0001 C CNN
F 1 "GND" H 10650 5850 50  0000 C CNN
F 2 "" H 10650 6000 50  0000 C CNN
F 3 "" H 10650 6000 50  0000 C CNN
F 4 "supp" H 450 850 60  0001 C CNN "supplier"
F 5 "supp#" H 450 850 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 450 850 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 450 850 60  0001 C CNN "manf#"
	1    10650 6000
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 57EE230F
P 10950 3150
F 0 "#PWR?" H 10950 3000 50  0001 C CNN
F 1 "+5V" H 10950 3290 50  0000 C CNN
F 2 "" H 10950 3150 50  0000 C CNN
F 3 "" H 10950 3150 50  0000 C CNN
F 4 "supp" H 2500 2500 60  0001 C CNN "supplier"
F 5 "supp#" H 2500 2500 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 2500 2500 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 2500 2500 60  0001 C CNN "manf#"
	1    10950 3150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 57EE46EB
P 6050 1100
F 0 "#PWR?" H 6050 950 50  0001 C CNN
F 1 "+5V" H 6050 1240 50  0000 C CNN
F 2 "" H 6050 1100 50  0000 C CNN
F 3 "" H 6050 1100 50  0000 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    6050 1100
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 57EE4720
P 1450 1150
F 0 "#PWR?" H 1450 1000 50  0001 C CNN
F 1 "+5V" H 1450 1290 50  0000 C CNN
F 2 "" H 1450 1150 50  0000 C CNN
F 3 "" H 1450 1150 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 1150
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R1
U 1 1 57F79B33
P 8500 4350
F 0 "R1" V 8580 4350 50  0000 C CNN
F 1 "1K" V 8500 4350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8430 4350 50  0001 C CNN
F 3 "" H 8500 4350 50  0000 C CNN
F 4 "supp" H 2950 350 60  0001 C CNN "supplier"
F 5 "supp#" H 2950 350 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 2950 350 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 2950 350 60  0001 C CNN "manf#"
	1    8500 4350
	1    0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 57F79B36
P 8400 5300
F 0 "#PWR?" H 8400 5050 50  0001 C CNN
F 1 "GND" H 8400 5150 50  0000 C CNN
F 2 "" H 8400 5300 50  0000 C CNN
F 3 "" H 8400 5300 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8400 5300
	-1   0    0    -1  
$EndComp
$Comp
L BC546 Q1
U 1 1 57FA0D1F
P 4250 5900
F 0 "Q1" H 4450 5975 50  0000 L CNN
F 1 "BC337" H 4450 5900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Molded_Wide" H 4450 5825 50  0001 L CIN
F 3 "" H 4250 5900 50  0000 L CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4250 5900
	-1   0    0    -1  
$EndComp
$Comp
L DIODE D1
U 1 1 57FA0F8B
P 3450 5250
F 0 "D1" H 3450 5400 50  0000 C CNN
F 1 "1N4001" H 3450 5075 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3450 5250 50  0001 C CNN
F 3 "" H 3450 5250 50  0000 C CNN
F 4 "Rapid" H 3450 5250 60  0001 C CNN "Supplier"
F 5 "£" H 3450 5250 60  0001 C CNN "Cost"
F 6 "supp" H 0   0   60  0001 C CNN "supplier"
F 7 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 8 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 9 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3450 5250
	0    1    -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 57FA15B4
P 4150 6250
F 0 "#PWR?" H 4150 6000 50  0001 C CNN
F 1 "GND" H 4150 6100 50  0000 C CNN
F 2 "" H 4150 6250 50  0000 C CNN
F 3 "" H 4150 6250 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4150 6250
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R2
U 1 1 57FA16C2
P 4600 5750
F 0 "R2" V 4680 5750 50  0000 C CNN
F 1 "1K" V 4600 5750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4530 5750 50  0001 C CNN
F 3 "" H 4600 5750 50  0000 C CNN
F 4 "supp" H 50  3350 60  0001 C CNN "supplier"
F 5 "supp#" H 50  3350 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 50  3350 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 50  3350 60  0001 C CNN "manf#"
	1    4600 5750
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 57FA1E61
P 3450 4700
F 0 "#PWR?" H 3450 4550 50  0001 C CNN
F 1 "+5V" H 3450 4840 50  0000 C CNN
F 2 "" H 3450 4700 50  0000 C CNN
F 3 "" H 3450 4700 50  0000 C CNN
F 4 "supp" H 0   250 60  0001 C CNN "supplier"
F 5 "supp#" H 0   250 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   250 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   250 60  0001 C CNN "manf#"
	1    3450 4700
	-1   0    0    -1  
$EndComp
$Comp
L AVR-ISP-6 ISP1
U 1 1 57FA4C9D
P 10850 3700
F 0 "ISP1" H 10745 3940 50  0000 C CNN
F 1 "ISP" H 10585 3470 50  0000 L BNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" V 10330 3740 50  0001 C CNN
F 3 "" H 10825 3700 50  0000 C CNN
F 4 "supp" H 7450 2700 60  0001 C CNN "supplier"
F 5 "supp#" H 7450 2700 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 7450 2700 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 7450 2700 60  0001 C CNN "manf#"
	1    10850 3700
	-1   0    0    -1  
$EndComp
$Comp
L Screw_Terminal_1x03 J1
U 1 1 58063FC8
P 1350 5150
F 0 "J1" H 1350 5500 50  0000 C TNN
F 1 "CTB5201/BK3" V 1200 5150 50  0000 C TNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 1350 4825 50  0001 C CNN
F 3 "" H 1325 5250 50  0001 C CNN
F 4 "Rapid" H 1350 5150 60  0001 C CNN "Supplier"
F 5 "£" H 1350 5150 60  0001 C CNN "Cost"
	1    1350 5150
	1    0    0    1   
$EndComp
$Comp
L DFPlayer_Mini-RESCUE-torchy U2
U 1 1 580F4ABF
P 7600 4850
F 0 "U2" H 7250 4300 50  0000 C CNN
F 1 "DFPlayer_Mini" V 7550 4750 50  0000 C CNN
F 2 "Oddities:DFPlayer_Mini" H 7500 4800 50  0001 C CNN
F 3 "" H 7500 4800 50  0000 C CNN
F 4 "£" H 7600 4850 60  0001 C CNN "Cost"
	1    7600 4850
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 580F53E1
P 8200 4350
F 0 "#PWR?" H 8200 4200 50  0001 C CNN
F 1 "+5V" H 8200 4490 50  0000 C CNN
F 2 "" H 8200 4350 50  0000 C CNN
F 3 "" H 8200 4350 50  0000 C CNN
F 4 "supp" H -200 0   60  0001 C CNN "supplier"
F 5 "supp#" H -200 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -200 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -200 0   60  0001 C CNN "manf#"
	1    8200 4350
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R5
U 1 1 580F5923
P 8450 4950
F 0 "R5" V 8500 4800 50  0000 C CNN
F 1 "1K" V 8450 4950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8380 4950 50  0001 C CNN
F 3 "" H 8450 4950 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8450 4950
	0    -1   1    0   
$EndComp
$Comp
L R-RESCUE-torchy R4
U 1 1 580F59CC
P 8450 4850
F 0 "R4" V 8400 4700 50  0000 C CNN
F 1 "1K" V 8450 4850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8380 4850 50  0001 C CNN
F 3 "" H 8450 4850 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    8450 4850
	0    -1   1    0   
$EndComp
$Comp
L R-RESCUE-torchy R8
U 1 1 5821FCB7
P 6950 4400
F 0 "R8" V 7030 4400 50  0000 C CNN
F 1 "1K" V 6950 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6880 4400 50  0001 C CNN
F 3 "" H 6950 4400 50  0000 C CNN
F 4 "supp" H 1400 -150 60  0001 C CNN "supplier"
F 5 "supp#" H 1400 -150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1400 -150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1400 -150 60  0001 C CNN "manf#"
	1    6950 4400
	1    0    0    1   
$EndComp
$Comp
L C_Small C1
U 1 1 582DA90F
P 1450 2050
F 0 "C1" H 1460 2120 50  0000 L CNN
F 1 "100nF" H 1460 1970 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 1450 2050 50  0001 C CNN
F 3 "" H 1450 2050 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 2050
	1    0    0    -1  
$EndComp
$Comp
L Screw_Terminal_1x03 J2
U 1 1 5896538A
P 650 7150
F 0 "J2" H 650 7500 50  0000 C TNN
F 1 "CTB5201/BK3" V 500 7150 50  0000 C TNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 650 6825 50  0001 C CNN
F 3 "" H 625 7250 50  0001 C CNN
F 4 "supp" H -100 0   60  0001 C CNN "supplier"
F 5 "supp#" H -100 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -100 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -100 0   60  0001 C CNN "manf#"
	1    650  7150
	1    0    0    1   
$EndComp
$Comp
L FUSE_Small F1
U 1 1 58965591
P 1200 6950
F 0 "F1" H 1160 7010 50  0000 L CNN
F 1 "FUSE_Small" H 1000 6950 50  0000 L CNN
F 2 "Fuse_Holders_and_Fuses:Fuse_TE5_Littlefuse-395Series" H 1200 6950 50  0001 C CNN
F 3 "" H 1200 6950 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1200 6950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58965804
P 2650 7450
F 0 "#PWR?" H 2650 7200 50  0001 C CNN
F 1 "GND" H 2650 7300 50  0000 C CNN
F 2 "" H 2650 7450 50  0000 C CNN
F 3 "" H 2650 7450 50  0000 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    2650 7450
	-1   0    0    -1  
$EndComp
$Comp
L +12V #PWR?
U 1 1 589658F4
P 2950 6850
F 0 "#PWR?" H 2950 6700 50  0001 C CNN
F 1 "+12V" H 2950 6990 50  0000 C CNN
F 2 "" H 2950 6850 50  0000 C CNN
F 3 "" H 2950 6850 50  0000 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    2950 6850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 58965CDD
P 3900 6400
F 0 "#PWR?" H 3900 6250 50  0001 C CNN
F 1 "+5V" H 3900 6540 50  0000 C CNN
F 2 "" H 3900 6400 50  0000 C CNN
F 3 "" H 3900 6400 50  0000 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    3900 6400
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-torchy R9
U 1 1 58966796
P 4150 7400
F 0 "R9" V 4230 7400 50  0000 C CNN
F 1 "1K" V 4150 7400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4080 7400 50  0001 C CNN
F 3 "" H 4150 7400 50  0000 C CNN
F 4 "supp" H 900 0   60  0001 C CNN "supplier"
F 5 "supp#" H 900 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 900 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 900 0   60  0001 C CNN "manf#"
	1    4150 7400
	0    -1   1    0   
$EndComp
$Comp
L LED_ALT D2
U 1 1 5896681A
P 4400 7200
F 0 "D2" H 4400 7300 50  0000 C CNN
F 1 "LED_OR" H 4400 7100 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 4400 7200 50  0001 C CNN
F 3 "" H 4400 7200 50  0001 C CNN
F 4 "supp" H 800 -50 60  0001 C CNN "supplier"
F 5 "supp#" H 800 -50 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 800 -50 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 800 -50 60  0001 C CNN "manf#"
	1    4400 7200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 58968497
P 10950 4150
F 0 "#PWR?" H 10950 3900 50  0001 C CNN
F 1 "GND" H 10950 4000 50  0000 C CNN
F 2 "" H 10950 4150 50  0000 C CNN
F 3 "" H 10950 4150 50  0000 C CNN
F 4 "supp" H 7900 2950 60  0001 C CNN "supplier"
F 5 "supp#" H 7900 2950 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 7900 2950 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 7900 2950 60  0001 C CNN "manf#"
	1    10950 4150
	-1   0    0    -1  
$EndComp
$Comp
L LM7805CT U3
U 1 1 5896FBB7
P 3450 7000
F 0 "U3" H 3250 7200 50  0000 C CNN
F 1 "LM7805CT" H 3450 7200 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 3450 7100 50  0001 C CIN
F 3 "" H 3450 7000 50  0001 C CNN
F 4 "Rapid" H 300 0   60  0001 C CNN "supplier"
F 5 "47-3313" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£0.327; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    3450 7000
	1    0    0    -1  
$EndComp
$Comp
L G5Q-1-RESCUE-torchy-tiny44 RL1
U 1 1 589737BD
P 3100 5450
F 0 "RL1" H 3750 5800 50  0000 L CNN
F 1 "G5Q-1" H 3750 5700 50  0000 L CNN
F 2 "Relays_THT:Relay_SANYOU_SRD_Series_Form_C" H 3750 5600 50  0001 L CNN
F 3 "" H 3300 5250 50  0001 C CNN
F 4 "60-4661" H 3100 5450 60  0001 C CNN "Supp ID"
F 5 "Rapid" H 3100 5450 60  0001 C CNN "Supplier"
F 6 "£" H 3100 5450 60  0001 C CNN "Cost"
	1    3100 5450
	-1   0    0    -1  
$EndComp
$Comp
L LED_ALT D3
U 1 1 58974449
P 3900 5450
F 0 "D3" H 3900 5550 50  0000 C CNN
F 1 "LED_R" H 3900 5350 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 3900 5450 50  0001 C CNN
F 3 "" H 3900 5450 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3900 5450
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-torchy R10
U 1 1 589744E1
P 3900 5050
F 0 "R10" V 3980 5050 50  0000 C CNN
F 1 "470R" V 3900 5050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3830 5050 50  0001 C CNN
F 3 "" H 3900 5050 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    3900 5050
	-1   0    0    -1  
$EndComp
$Comp
L TEST GND1
U 1 1 589A26DC
P 1000 2200
F 0 "GND1" H 1000 2500 50  0000 C BNN
F 1 "TEST" H 1000 2450 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 1000 2200 50  0001 C CNN
F 3 "" H 1000 2200 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1000 2200
	1    0    0    -1  
$EndComp
$Comp
L TEST TP2
U 1 1 589A2A2F
P 5500 1400
F 0 "TP2" H 5500 1700 50  0000 C BNN
F 1 "TEST" H 5500 1650 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 5500 1400 50  0001 C CNN
F 3 "" H 5500 1400 50  0001 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    5500 1400
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 589A5D32
P 2950 7100
F 0 "C2" H 2975 7200 50  0000 L CNN
F 1 "100uF" H 2975 7000 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2988 6950 50  0001 C CNN
F 3 "" H 2950 7100 50  0001 C CNN
F 4 "supp" H 300 0   60  0001 C CNN "supplier"
F 5 "supp#" H 300 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 300 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 300 0   60  0001 C CNN "manf#"
	1    2950 7100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 J3
U 1 1 58B1B3D4
P 6350 1600
F 0 "J3" H 6350 1800 50  0000 C CNN
F 1 "CONN_01X03" V 6450 1600 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-03A_1x03_P2.54mm_Vertical" H 6350 1600 50  0001 C CNN
F 3 "" H 6350 1600 50  0001 C CNN
F 4 "supp" H 350 100 60  0001 C CNN "supplier"
F 5 "supp#" H 350 100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 350 100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 350 100 60  0001 C CNN "manf#"
	1    6350 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58B1B46F
P 5450 2350
F 0 "#PWR?" H 5450 2100 50  0001 C CNN
F 1 "GND" H 5450 2200 50  0000 C CNN
F 2 "" H 5450 2350 50  0000 C CNN
F 3 "" H 5450 2350 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5450 2350
	-1   0    0    -1  
$EndComp
Text Notes 6200 1350 0    50   ~ 0
OFF-BOARD \nSENSORS
$Comp
L CONN_01X05 J5
U 1 1 594EC30D
P 10300 2150
F 0 "J5" H 10300 2450 50  0000 C CNN
F 1 "DS3231 RTC" V 10400 2150 50  0000 C CNN
F 2 "Mike:DS3231" H 10300 2150 50  0001 C CNN
F 3 "" H 10300 2150 50  0001 C CNN
F 4 "supp" H 1750 50  60  0001 C CNN "supplier"
F 5 "supp#" H 1750 50  60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1750 50  60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1750 50  60  0001 C CNN "manf#"
	1    10300 2150
	1    0    0    -1  
$EndComp
$Comp
L ATTINY44-20PU U4
U 1 1 59662196
P 2750 2100
F 0 "U4" H 1900 2850 50  0000 C CNN
F 1 "ATTINY44-20PU" H 3450 1350 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm" H 2750 1900 50  0001 C CIN
F 3 "" H 2750 2100 50  0001 C CNN
F 4 "73-5118" H 2750 2100 60  0001 C CNN "Supp ID"
F 5 "Rapid" H 2750 2100 60  0001 C CNN "Supplier"
F 6 "£2.11" H 0   0   60  0001 C CNN "Cost"
	1    2750 2100
	1    0    0    -1  
$EndComp
$Comp
L POT_TRIM RV1
U 1 1 596734F6
P 5200 2350
F 0 "RV1" V 5025 2350 50  0000 C CNN
F 1 "POT_TRIM" V 5100 2350 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 5200 2350 50  0001 C CNN
F 3 "" H 5200 2350 50  0001 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    5200 2350
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR?
U 1 1 596737D3
P 4950 2350
F 0 "#PWR?" H 4950 2200 50  0001 C CNN
F 1 "+5V" H 4950 2490 50  0000 C CNN
F 2 "" H 4950 2350 50  0000 C CNN
F 3 "" H 4950 2350 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    4950 2350
	-1   0    0    -1  
$EndComp
$Comp
L POT_TRIM RV2
U 1 1 5967415E
P 5050 1350
F 0 "RV2" V 4875 1350 50  0000 C CNN
F 1 "POT_TRIM" V 4950 1350 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 5050 1350 50  0001 C CNN
F 3 "" H 5050 1350 50  0001 C CNN
F 4 "supp" H 950 0   60  0001 C CNN "supplier"
F 5 "supp#" H 950 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 950 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 950 0   60  0001 C CNN "manf#"
	1    5050 1350
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR?
U 1 1 5967D5B8
P 5250 1350
F 0 "#PWR?" H 5250 1200 50  0001 C CNN
F 1 "+5V" H 5250 1490 50  0000 C CNN
F 2 "" H 5250 1350 50  0000 C CNN
F 3 "" H 5250 1350 50  0000 C CNN
F 4 "supp" H 950 0   60  0001 C CNN "supplier"
F 5 "supp#" H 950 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 950 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 950 0   60  0001 C CNN "manf#"
	1    5250 1350
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X04 J6
U 1 1 596DE49C
P 8200 2050
F 0 "J6" H 8200 2300 50  0000 C CNN
F 1 "CONN_01X04" V 8300 2050 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8200 2050 50  0001 C CNN
F 3 "" H 8200 2050 50  0001 C CNN
	1    8200 2050
	1    0    0    1   
$EndComp
$Comp
L +5V #PWR?
U 1 1 596DE56B
P 8000 1750
F 0 "#PWR?" H 8000 1600 50  0001 C CNN
F 1 "+5V" H 8000 1890 50  0000 C CNN
F 2 "" H 8000 1750 50  0000 C CNN
F 3 "" H 8000 1750 50  0000 C CNN
F 4 "supp" H 3050 -600 60  0001 C CNN "supplier"
F 5 "supp#" H 3050 -600 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 3050 -600 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 3050 -600 60  0001 C CNN "manf#"
	1    8000 1750
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 596DE5F0
P 8000 2350
F 0 "#PWR?" H 8000 2100 50  0001 C CNN
F 1 "GND" H 8000 2200 50  0000 C CNN
F 2 "" H 8000 2350 50  0000 C CNN
F 3 "" H 8000 2350 50  0000 C CNN
F 4 "supp" H 2550 0   60  0001 C CNN "supplier"
F 5 "supp#" H 2550 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 2550 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 2550 0   60  0001 C CNN "manf#"
	1    8000 2350
	-1   0    0    -1  
$EndComp
NoConn ~ 8100 5050
NoConn ~ 8100 5250
NoConn ~ 7150 5250
NoConn ~ 7150 5050
NoConn ~ 7150 4950
NoConn ~ 7150 4850
NoConn ~ 7150 4750
NoConn ~ 7150 4650
$Comp
L GND #PWR?
U 1 1 596E8C4F
P 7000 5350
F 0 "#PWR?" H 7000 5100 50  0001 C CNN
F 1 "GND" H 7000 5200 50  0000 C CNN
F 2 "" H 7000 5350 50  0000 C CNN
F 3 "" H 7000 5350 50  0000 C CNN
F 4 "supp" H -1400 50  60  0001 C CNN "supplier"
F 5 "supp#" H -1400 50  60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1400 50  60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1400 50  60  0001 C CNN "manf#"
	1    7000 5350
	-1   0    0    -1  
$EndComp
$Comp
L Jumper_NO_Small JP1
U 1 1 596F5C8C
P 2550 7050
F 0 "JP1" H 2550 7130 50  0000 C CNN
F 1 "TEST PWR" V 2560 6990 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2550 7050 50  0001 C CNN
F 3 "" H 2550 7050 50  0001 C CNN
	1    2550 7050
	0    -1   -1   0   
$EndComp
Text GLabel 3900 1800 2    55   BiDi ~ 0
AUDIO
Text GLabel 3950 2400 2    55   Output ~ 0
RELAY
$Comp
L R-RESCUE-torchy R7
U 1 1 59970B76
P 8450 4750
F 0 "R7" V 8400 4600 50  0000 C CNN
F 1 "1K" V 8450 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8380 4750 50  0001 C CNN
F 3 "" H 8450 4750 50  0000 C CNN
F 4 "supp" H 0   -100 60  0001 C CNN "supplier"
F 5 "supp#" H 0   -100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   -100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   -100 60  0001 C CNN "manf#"
	1    8450 4750
	0    -1   1    0   
$EndComp
$Comp
L +12V #PWR?
U 1 1 599F00D8
P 8900 4550
F 0 "#PWR?" H 8900 4400 50  0001 C CNN
F 1 "+12V" H 8900 4690 50  0000 C CNN
F 2 "" H 8900 4550 50  0000 C CNN
F 3 "" H 8900 4550 50  0000 C CNN
F 4 "supp" H 750 1150 60  0001 C CNN "supplier"
F 5 "supp#" H 750 1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 750 1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 750 1150 60  0001 C CNN "manf#"
	1    8900 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1150 1450 1950
Wire Wire Line
	1450 1800 1650 1800
Wire Wire Line
	1000 2300 1650 2300
Wire Wire Line
	4150 6100 4150 6250
Wire Wire Line
	3450 5450 3450 5700
Wire Wire Line
	3100 5700 4150 5700
Wire Wire Line
	3100 5700 3100 5550
Connection ~ 3450 5700
Wire Wire Line
	8200 4350 8200 4600
Wire Wire Line
	8200 4550 8100 4550
Wire Wire Line
	8400 5300 8400 5150
Wire Wire Line
	8950 5150 8100 5150
Wire Wire Line
	8100 4850 8300 4850
Wire Wire Line
	8300 4950 8100 4950
Wire Wire Line
	8100 4650 8500 4650
Wire Wire Line
	6950 4000 8500 4000
Connection ~ 1450 1800
Wire Wire Line
	1450 2150 1450 2600
Wire Wire Line
	2200 7250 3450 7250
Connection ~ 2650 7250
Wire Wire Line
	3850 6950 4400 6950
Connection ~ 1450 2300
Connection ~ 3900 6950
Wire Wire Line
	2650 7400 4000 7400
Connection ~ 2650 7400
Wire Wire Line
	900  5550 1650 5550
Wire Wire Line
	1650 5550 1650 5150
Wire Wire Line
	1650 5150 1550 5150
Wire Wire Line
	3100 4950 3100 4850
Wire Wire Line
	3100 4850 3900 4850
Wire Wire Line
	3450 4700 3450 5050
Connection ~ 3450 4850
Wire Wire Line
	4600 5900 4450 5900
Wire Wire Line
	2700 6700 2700 5550
Wire Wire Line
	2600 4950 2600 4850
Wire Wire Line
	2600 4850 1700 4850
Wire Wire Line
	1700 4850 1700 4950
Wire Wire Line
	1700 4950 1550 4950
Wire Wire Line
	3900 4850 3900 4900
Wire Wire Line
	3900 5200 3900 5300
Wire Wire Line
	3900 5600 3900 5700
Connection ~ 3900 5700
Wire Wire Line
	1000 2200 1000 2300
Wire Wire Line
	2200 6950 3050 6950
Connection ~ 2950 6950
Connection ~ 2950 7250
Wire Wire Line
	2950 6950 2950 6850
Wire Wire Line
	6150 1500 6050 1500
Wire Wire Line
	6050 1500 6050 1100
Wire Wire Line
	3800 2700 3950 2700
Wire Wire Line
	1650 1800 1650 1500
Wire Wire Line
	1650 1500 1700 1500
Wire Wire Line
	1650 2300 1650 2700
Wire Wire Line
	1650 2700 1700 2700
Wire Wire Line
	3900 2000 3800 2000
Wire Wire Line
	3800 1900 4200 1900
Wire Wire Line
	5500 1400 5500 1600
Wire Wire Line
	3800 2100 4200 2100
Wire Wire Line
	3800 1600 6150 1600
Wire Wire Line
	3800 1700 6150 1700
Wire Wire Line
	3800 2200 5200 2200
Wire Wire Line
	5350 2350 5450 2350
Connection ~ 4950 2350
Wire Wire Line
	4950 2350 5050 2350
Wire Wire Line
	3800 1500 5050 1500
Wire Wire Line
	4800 1350 4900 1350
Wire Wire Line
	5200 1350 5250 1350
Wire Wire Line
	8000 1900 8000 1750
Wire Wire Line
	8000 2200 8000 2350
Wire Wire Line
	7150 5150 7000 5150
Wire Wire Line
	7000 5150 7000 5350
Wire Wire Line
	2650 7250 2650 7450
Wire Wire Line
	2200 7150 2200 7250
Connection ~ 2550 7250
Connection ~ 2550 6950
Wire Wire Line
	2550 7150 2550 7250
Wire Wire Line
	950  6700 2700 6700
Connection ~ 5500 1600
Wire Wire Line
	8100 4750 8300 4750
Wire Wire Line
	8600 4750 8800 4750
Wire Wire Line
	8800 4750 8800 5150
Connection ~ 8400 5150
$Comp
L +5V #PWR?
U 1 1 594E7530
P 10100 1600
F 0 "#PWR?" H 10100 1450 50  0001 C CNN
F 1 "+5V" H 10100 1740 50  0000 C CNN
F 2 "" H 10100 1600 50  0000 C CNN
F 3 "" H 10100 1600 50  0000 C CNN
F 4 "supp" H 1750 100 60  0001 C CNN "supplier"
F 5 "supp#" H 1750 100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1750 100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1750 100 60  0001 C CNN "manf#"
	1    10100 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10100 1600 10100 1950
$Comp
L GND #PWR?
U 1 1 594E7491
P 10100 2500
F 0 "#PWR?" H 10100 2250 50  0001 C CNN
F 1 "GND" H 10100 2350 50  0000 C CNN
F 2 "" H 10100 2500 50  0000 C CNN
F 3 "" H 10100 2500 50  0000 C CNN
F 4 "supp" H 1750 -300 60  0001 C CNN "supplier"
F 5 "supp#" H 1750 -300 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1750 -300 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1750 -300 60  0001 C CNN "manf#"
	1    10100 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10100 2350 10100 2500
NoConn ~ 10100 2250
Wire Wire Line
	8500 4000 8500 4200
Connection ~ 6950 4000
Wire Wire Line
	6950 4550 7150 4550
Wire Wire Line
	8500 4650 8500 4500
$Comp
L +5V #PWR?
U 1 1 59A167E6
P 4200 3250
F 0 "#PWR?" H 4200 3100 50  0001 C CNN
F 1 "+5V" H 4200 3390 50  0000 C CNN
F 2 "" H 4200 3250 50  0000 C CNN
F 3 "" H 4200 3250 50  0000 C CNN
F 4 "supp" H 750 -1200 60  0001 C CNN "supplier"
F 5 "supp#" H 750 -1200 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 750 -1200 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 750 -1200 60  0001 C CNN "manf#"
	1    4200 3250
	-1   0    0    -1  
$EndComp
$Comp
L TIANBO_HJR_1-2C K1
U 1 1 59A1820D
P 4000 3850
F 0 "K1" H 3950 4150 50  0000 L CNN
F 1 "TIANBO_HJR_1-2C" H 3900 3550 50  0000 L CNN
F 2 "Relays_THT:Relay_DPDT_HJR_1-2C" H 5550 3820 50  0001 C CNN
F 3 "" H 4000 3850 50  0001 C CNN
	1    4000 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4200 3250 4200 3550
Connection ~ 4200 3350
Text GLabel 3900 1600 2    55   Input ~ 0
PCINT1
Text GLabel 4950 1600 2    55   Input ~ 0
ADC1
$Comp
L CP C3
U 1 1 59AFEEA7
P 3900 7200
F 0 "C3" H 3925 7300 50  0000 L CNN
F 1 "100uF" H 3925 7100 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 3938 7050 50  0001 C CNN
F 3 "" H 3900 7200 50  0001 C CNN
F 4 "supp" H 1250 100 60  0001 C CNN "supplier"
F 5 "supp#" H 1250 100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1250 100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1250 100 60  0001 C CNN "manf#"
	1    3900 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 6400 3900 7050
Wire Wire Line
	4400 6950 4400 7050
Wire Wire Line
	4400 7350 4400 7400
Wire Wire Line
	4400 7400 4300 7400
Wire Wire Line
	3900 7400 3900 7350
Connection ~ 3900 7400
$Comp
L CONN_01X04 J4
U 1 1 59FB1329
P 8800 3400
F 0 "J4" H 8800 3650 50  0000 C CNN
F 1 "SPKR" V 8900 3400 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-4_P5.08mm" H 8800 3400 50  0001 C CNN
F 3 "" H 8800 3400 50  0001 C CNN
	1    8800 3400
	0    1    -1   0   
$EndComp
Wire Wire Line
	4200 5450 4150 5450
Wire Wire Line
	4150 5450 4150 5700
Connection ~ 4200 4400
Wire Wire Line
	4200 4150 4200 5450
$Comp
L IRM-02-12 U5
U 1 1 5A44F8BB
P 1800 7050
F 0 "U5" H 2200 7300 50  0000 C CNN
F 1 "IRM-02-12" H 2200 6900 50  0000 C CNN
F 2 "Converter_ACDC:Converter_ACDC_MeanWell_IRM-10-xx_THT" H 2200 6800 50  0001 C CNN
F 3 "" H 2200 6700 50  0001 C CNN
	1    1800 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3700 10450 3700
Wire Wire Line
	10950 3150 10950 3200
Wire Wire Line
	10950 4150 10950 4100
$Comp
L YL2020 U1
U 1 1 5A4A4963
P 9350 4400
F 0 "U1" H 8800 4600 50  0000 C CNN
F 1 "YL2020" H 9800 4600 50  0000 C CNN
F 2 "Mikee:YL2020-v1.0-VERT" H 9350 4400 50  0001 C CNN
F 3 "" H 9350 4400 50  0001 C CNN
	1    9350 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	8950 4850 9100 4850
Wire Wire Line
	8950 4650 8950 5150
Connection ~ 8800 5150
Wire Wire Line
	9100 4950 8600 4950
Wire Wire Line
	8600 4850 8900 4850
Wire Wire Line
	8900 4850 8900 4750
Wire Wire Line
	8900 4750 9100 4750
Wire Wire Line
	8950 4650 9100 4650
Connection ~ 8950 4850
Wire Wire Line
	8900 4550 9100 4550
Wire Wire Line
	8950 3600 8950 3850
Wire Wire Line
	8950 3850 9100 3850
Wire Wire Line
	8850 3600 8850 3950
Wire Wire Line
	8850 3950 9100 3950
Wire Wire Line
	8750 3600 8750 4050
Wire Wire Line
	8750 4050 9100 4050
Wire Wire Line
	8650 3600 8650 4150
Wire Wire Line
	8650 4150 9100 4150
Wire Wire Line
	9100 4250 8950 4250
Wire Wire Line
	8950 4250 8950 4550
Connection ~ 8950 4550
Wire Wire Line
	8250 4350 9100 4350
Wire Wire Line
	8250 4350 8250 4450
Wire Wire Line
	8250 4450 7100 4450
Wire Wire Line
	7100 4450 7100 4550
Connection ~ 7100 4550
Text GLabel 4200 1900 2    45   Output ~ 0
I2C_CLK
Text GLabel 4200 2100 2    45   BiDi ~ 0
I2C_DATA
Text GLabel 9500 2050 0    45   Input ~ 0
I2C_DATA
Text GLabel 9500 2150 0    45   Input ~ 0
I2C_CLK
Wire Wire Line
	9500 2050 10850 2050
Wire Wire Line
	9500 2150 10850 2150
Text GLabel 3900 2000 2    45   Input ~ 0
MISO
Text GLabel 3900 2100 2    45   Output ~ 0
MOSI
Text GLabel 3900 1900 2    45   Output ~ 0
CLK
Text GLabel 3950 2700 2    45   Output ~ 0
/RESET
Text GLabel 10350 3800 0    45   Input ~ 0
/RESET
Wire Wire Line
	10350 3800 10450 3800
Text GLabel 10350 3700 0    45   Input ~ 0
CLK
Text GLabel 10350 3600 0    45   Input ~ 0
MOSI
Text GLabel 10350 3500 0    45   Input ~ 0
MISO
Wire Wire Line
	10350 3600 10450 3600
Wire Wire Line
	10450 3500 10350 3500
Text GLabel 4600 5450 1    55   Input ~ 0
RELAY
Wire Wire Line
	4600 5450 4600 5600
Wire Wire Line
	3800 2400 3950 2400
Text GLabel 3950 2500 2    45   Input ~ 0
SP_1
Text GLabel 3950 2600 2    45   Input ~ 0
SP_2
Text GLabel 7900 2000 0    45   Output ~ 0
SP_1
Text GLabel 7900 2100 0    45   Output ~ 0
SP_2
Wire Wire Line
	7900 2000 8000 2000
Wire Wire Line
	7900 2100 8000 2100
Wire Wire Line
	3800 2500 3950 2500
Wire Wire Line
	3800 2600 3950 2600
Wire Wire Line
	3800 1800 3900 1800
Text GLabel 6950 4000 0    55   BiDi ~ 0
AUDIO
Wire Wire Line
	6950 4250 6950 4000
$Comp
L Mounting_Hole MK1
U 1 1 5A610D67
P 5400 7550
F 0 "MK1" H 5400 7750 50  0000 C CNN
F 1 "Hole" H 5400 7675 50  0000 C CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 5400 7550 50  0001 C CNN
F 3 "" H 5400 7550 50  0001 C CNN
	1    5400 7550
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole MK2
U 1 1 5A61106B
P 5650 7550
F 0 "MK2" H 5650 7750 50  0000 C CNN
F 1 "Hole" H 5650 7675 50  0000 C CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 5650 7550 50  0001 C CNN
F 3 "" H 5650 7550 50  0001 C CNN
	1    5650 7550
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 J9
U 1 1 5A644D2C
P 10700 2100
F 0 "J9" H 10700 2350 50  0000 C CNN
F 1 "CONN_LCD" V 10800 2100 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 10700 2100 50  0001 C CNN
F 3 "" H 10700 2100 50  0001 C CNN
	1    10700 2100
	1    0    0    1   
$EndComp
Wire Wire Line
	10100 1950 10850 1950
Connection ~ 10100 2050
Connection ~ 10100 2150
Wire Wire Line
	10100 2350 10500 2350
Wire Wire Line
	10500 2350 10500 2250
$Comp
L CONN_01X04 J10
U 1 1 5A646ACE
P 11050 2100
F 0 "J10" H 11050 2350 50  0000 C CNN
F 1 "CONN_KEYBD" V 11150 2100 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 11050 2100 50  0001 C CNN
F 3 "" H 11050 2100 50  0001 C CNN
	1    11050 2100
	1    0    0    1   
$EndComp
Connection ~ 10500 1950
Connection ~ 10500 2050
Connection ~ 10500 2150
Wire Wire Line
	10500 2250 10850 2250
$Comp
L DIODE D4
U 1 1 5A649694
P 4650 3800
F 0 "D4" H 4650 3950 50  0000 C CNN
F 1 "1N4001" H 4650 3625 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4650 3800 50  0001 C CNN
F 3 "" H 4650 3800 50  0000 C CNN
F 4 "Rapid" H 4650 3800 60  0001 C CNN "Supplier"
F 5 "£" H 4650 3800 60  0001 C CNN "Cost"
F 6 "supp" H 1200 -1450 60  0001 C CNN "supplier"
F 7 "supp#" H 1200 -1450 60  0001 C CNN "supplier#"
F 8 "1:£; 10:£; 25:£; 100:£" H 1200 -1450 60  0001 C CNN "kicost:pricing"
F 9 "manf#" H 1200 -1450 60  0001 C CNN "manf#"
	1    4650 3800
	0    1    -1   0   
$EndComp
Wire Wire Line
	4200 3350 4650 3350
Wire Wire Line
	4650 3350 4650 3600
Wire Wire Line
	4650 4000 4650 4400
Wire Wire Line
	4650 4400 4200 4400
$Comp
L R R3
U 1 1 5B362DC3
P 9650 1800
F 0 "R3" V 9730 1800 50  0000 C CNN
F 1 "4K7" V 9650 1800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9580 1800 50  0001 C CNN
F 3 "" H 9650 1800 50  0001 C CNN
	1    9650 1800
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5B362E77
P 9800 1800
F 0 "R6" V 9880 1800 50  0000 C CNN
F 1 "4K7" V 9800 1800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9730 1800 50  0001 C CNN
F 3 "" H 9800 1800 50  0001 C CNN
	1    9800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 1950 9650 2050
Connection ~ 9650 2050
Wire Wire Line
	9800 1950 9800 2150
Connection ~ 9800 2150
Wire Wire Line
	9650 1650 10100 1650
Connection ~ 10100 1650
Connection ~ 9800 1650
Wire Wire Line
	2450 4300 3800 4300
Wire Wire Line
	850  6950 1100 6950
Wire Wire Line
	950  6950 950  6700
Connection ~ 950  6950
Wire Wire Line
	850  7150 900  7150
Wire Wire Line
	900  7150 900  5550
Wire Wire Line
	850  7350 2050 7350
Wire Wire Line
	1550 5350 2050 5350
Wire Wire Line
	2050 5350 2050 7350
Connection ~ 1400 7350
Wire Wire Line
	1300 6950 1400 6950
Wire Wire Line
	1400 7350 1400 7150
$Comp
L C_Small C5
U 1 1 5B38CE93
P 8850 4600
F 0 "C5" H 8860 4670 50  0000 L CNN
F 1 "100nF" H 8860 4520 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 8850 4600 50  0001 C CNN
F 3 "" H 8850 4600 50  0000 C CNN
F 4 "supp" H 7400 2550 60  0001 C CNN "supplier"
F 5 "supp#" H 7400 2550 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 7400 2550 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 7400 2550 60  0001 C CNN "manf#"
	1    8850 4600
	1    0    0    -1  
$EndComp
$Comp
L C_Small C4
U 1 1 5B38D08D
P 8200 4700
F 0 "C4" H 8210 4770 50  0000 L CNN
F 1 "100nF" H 8210 4620 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 8200 4700 50  0001 C CNN
F 3 "" H 8200 4700 50  0000 C CNN
F 4 "supp" H 6750 2650 60  0001 C CNN "supplier"
F 5 "supp#" H 6750 2650 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 6750 2650 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 6750 2650 60  0001 C CNN "manf#"
	1    8200 4700
	1    0    0    -1  
$EndComp
Connection ~ 8200 4550
Wire Wire Line
	8200 4800 8200 5150
Connection ~ 8200 5150
Wire Wire Line
	8850 4700 8950 4700
Connection ~ 8950 4700
Wire Wire Line
	8850 4500 8950 4500
Connection ~ 8950 4500
NoConn ~ 9100 4450
$Comp
L Screw_Terminal_1x03 J7
U 1 1 5B39FF60
P 1950 3700
F 0 "J7" H 1950 4050 50  0000 C TNN
F 1 "CTB5201/BK3" V 1800 3700 50  0000 C TNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 1950 3375 50  0001 C CNN
F 3 "" H 1925 3800 50  0001 C CNN
F 4 "Rapid" H 1950 3700 60  0001 C CNN "Supplier"
F 5 "£" H 1950 3700 60  0001 C CNN "Cost"
	1    1950 3700
	1    0    0    1   
$EndComp
Wire Wire Line
	3800 4300 3800 4150
Wire Wire Line
	3300 3550 3300 3250
Wire Wire Line
	2350 3250 3700 3250
Wire Wire Line
	3700 3250 3700 3550
Wire Wire Line
	3500 3550 3500 3350
Wire Wire Line
	2450 3350 3900 3350
Wire Wire Line
	3900 3350 3900 3550
Wire Wire Line
	2450 4300 2450 3900
Wire Wire Line
	2450 3900 2150 3900
Wire Wire Line
	2150 3500 2350 3500
Wire Wire Line
	2350 3500 2350 3250
Connection ~ 3300 3250
Wire Wire Line
	2450 3350 2450 3700
Wire Wire Line
	2450 3700 2150 3700
Connection ~ 3500 3350
Wire Wire Line
	3400 4300 3400 4150
Connection ~ 3400 4300
$EndSCHEMATC
