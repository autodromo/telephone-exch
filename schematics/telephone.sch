EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "TELEPHONE SIM. CONTROLLER"
Date "2022-04-24"
Rev "3"
Comp "Millstream Computing"
Comment1 "(C) 2020"
Comment2 "For tank museum Bovington"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L telephone-rescue:GND #PWR?
U 1 1 57E53B41
P 1450 2600
F 0 "#PWR?" H 1450 2350 50  0001 C CNN
F 1 "GND" H 1450 2450 50  0000 C CNN
F 2 "" H 1450 2600 50  0000 C CNN
F 3 "" H 1450 2600 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 2600
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 57EE230F
P 5000 1100
F 0 "#PWR?" H 5000 950 50  0001 C CNN
F 1 "+5V" H 5000 1240 50  0000 C CNN
F 2 "" H 5000 1100 50  0000 C CNN
F 3 "" H 5000 1100 50  0000 C CNN
F 4 "supp" H -3450 450 60  0001 C CNN "supplier"
F 5 "supp#" H -3450 450 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -3450 450 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -3450 450 60  0001 C CNN "manf#"
	1    5000 1100
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 57EE4720
P 1450 1150
F 0 "#PWR?" H 1450 1000 50  0001 C CNN
F 1 "+5V" H 1450 1290 50  0000 C CNN
F 2 "" H 1450 1150 50  0000 C CNN
F 3 "" H 1450 1150 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 1150
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R1
U 1 1 57F79B33
P 9300 5200
F 0 "R1" V 9380 5200 50  0000 C CNN
F 1 "1K" V 9300 5200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9230 5200 50  0001 C CNN
F 3 "" H 9300 5200 50  0000 C CNN
F 4 "supp" H 3750 1200 60  0001 C CNN "supplier"
F 5 "supp#" H 3750 1200 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 3750 1200 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 3750 1200 60  0001 C CNN "manf#"
	1    9300 5200
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 57F79B36
P 9200 6150
F 0 "#PWR?" H 9200 5900 50  0001 C CNN
F 1 "GND" H 9200 6000 50  0000 C CNN
F 2 "" H 9200 6150 50  0000 C CNN
F 3 "" H 9200 6150 50  0000 C CNN
F 4 "supp" H 800 850 60  0001 C CNN "supplier"
F 5 "supp#" H 800 850 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 800 850 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 800 850 60  0001 C CNN "manf#"
	1    9200 6150
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:DFPlayer_Mini-RESCUE-torchy U2
U 1 1 580F4ABF
P 8400 5700
F 0 "U2" H 8050 5150 50  0000 C CNN
F 1 "DFPlayer_Mini" V 8350 5600 50  0000 C CNN
F 2 "Mikee:DFPlayer_Mini" H 8300 5650 50  0001 C CNN
F 3 "" H 8300 5650 50  0000 C CNN
F 4 "£" H 8400 5700 60  0001 C CNN "Cost"
	1    8400 5700
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 580F53E1
P 9000 5200
F 0 "#PWR?" H 9000 5050 50  0001 C CNN
F 1 "+5V" H 9000 5340 50  0000 C CNN
F 2 "" H 9000 5200 50  0000 C CNN
F 3 "" H 9000 5200 50  0000 C CNN
F 4 "supp" H 600 850 60  0001 C CNN "supplier"
F 5 "supp#" H 600 850 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 600 850 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 600 850 60  0001 C CNN "manf#"
	1    9000 5200
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R5
U 1 1 580F5923
P 9250 5800
F 0 "R5" V 9300 5650 50  0000 C CNN
F 1 "1K" V 9250 5800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9180 5800 50  0001 C CNN
F 3 "" H 9250 5800 50  0000 C CNN
F 4 "supp" H 800 850 60  0001 C CNN "supplier"
F 5 "supp#" H 800 850 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 800 850 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 800 850 60  0001 C CNN "manf#"
	1    9250 5800
	0    -1   1    0   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R4
U 1 1 580F59CC
P 9250 5700
F 0 "R4" V 9200 5550 50  0000 C CNN
F 1 "1K" V 9250 5700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9180 5700 50  0001 C CNN
F 3 "" H 9250 5700 50  0000 C CNN
F 4 "supp" H 800 850 60  0001 C CNN "supplier"
F 5 "supp#" H 800 850 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 800 850 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 800 850 60  0001 C CNN "manf#"
	1    9250 5700
	0    -1   1    0   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R8
U 1 1 5821FCB7
P 7750 5250
F 0 "R8" V 7830 5250 50  0000 C CNN
F 1 "1K" V 7750 5250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7680 5250 50  0001 C CNN
F 3 "" H 7750 5250 50  0000 C CNN
F 4 "supp" H 2200 700 60  0001 C CNN "supplier"
F 5 "supp#" H 2200 700 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 2200 700 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 2200 700 60  0001 C CNN "manf#"
	1    7750 5250
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:C_Small C1
U 1 1 582DA90F
P 1450 2050
F 0 "C1" H 1460 2120 50  0000 L CNN
F 1 "100nF" H 1460 1970 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 1450 2050 50  0001 C CNN
F 3 "" H 1450 2050 50  0000 C CNN
F 4 "supp" H 0   0   60  0001 C CNN "supplier"
F 5 "supp#" H 0   0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 0   0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 0   0   60  0001 C CNN "manf#"
	1    1450 2050
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 58965804
P 1650 7450
F 0 "#PWR?" H 1650 7200 50  0001 C CNN
F 1 "GND" H 1650 7300 50  0000 C CNN
F 2 "" H 1650 7450 50  0000 C CNN
F 3 "" H 1650 7450 50  0000 C CNN
F 4 "supp" H -700 0   60  0001 C CNN "supplier"
F 5 "supp#" H -700 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -700 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -700 0   60  0001 C CNN "manf#"
	1    1650 7450
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:+12V #PWR?
U 1 1 589658F4
P 1950 6850
F 0 "#PWR?" H 1950 6700 50  0001 C CNN
F 1 "+12V" H 1950 6990 50  0000 C CNN
F 2 "" H 1950 6850 50  0000 C CNN
F 3 "" H 1950 6850 50  0000 C CNN
F 4 "supp" H -700 0   60  0001 C CNN "supplier"
F 5 "supp#" H -700 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -700 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -700 0   60  0001 C CNN "manf#"
	1    1950 6850
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 58965CDD
P 2900 6400
F 0 "#PWR?" H 2900 6250 50  0001 C CNN
F 1 "+5V" H 2900 6540 50  0000 C CNN
F 2 "" H 2900 6400 50  0000 C CNN
F 3 "" H 2900 6400 50  0000 C CNN
F 4 "supp" H -700 0   60  0001 C CNN "supplier"
F 5 "supp#" H -700 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -700 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -700 0   60  0001 C CNN "manf#"
	1    2900 6400
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R9
U 1 1 58966796
P 3150 7400
F 0 "R9" V 3230 7400 50  0000 C CNN
F 1 "1K" V 3150 7400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3080 7400 50  0001 C CNN
F 3 "" H 3150 7400 50  0000 C CNN
F 4 "supp" H -100 0   60  0001 C CNN "supplier"
F 5 "supp#" H -100 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -100 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -100 0   60  0001 C CNN "manf#"
	1    3150 7400
	0    -1   1    0   
$EndComp
$Comp
L telephone-rescue:LED_ALT D2
U 1 1 5896681A
P 3400 7200
F 0 "D2" H 3400 7300 50  0000 C CNN
F 1 "LED_OR" H 3400 7100 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 3400 7200 50  0001 C CNN
F 3 "" H 3400 7200 50  0001 C CNN
F 4 "supp" H -200 -50 60  0001 C CNN "supplier"
F 5 "supp#" H -200 -50 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -200 -50 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -200 -50 60  0001 C CNN "manf#"
	1    3400 7200
	0    -1   -1   0   
$EndComp
$Comp
L telephone-rescue:CP C2
U 1 1 589A5D32
P 1950 7100
F 0 "C2" H 1975 7200 50  0000 L CNN
F 1 "100uF" H 1975 7000 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H5.0mm_P2.50mm" H 1988 6950 50  0001 C CNN
F 3 "" H 1950 7100 50  0001 C CNN
F 4 "supp" H -700 0   60  0001 C CNN "supplier"
F 5 "supp#" H -700 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -700 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -700 0   60  0001 C CNN "manf#"
	1    1950 7100
	1    0    0    -1  
$EndComp
NoConn ~ 8900 5900
NoConn ~ 8900 6100
NoConn ~ 7950 6100
NoConn ~ 7950 5900
NoConn ~ 7950 5800
NoConn ~ 7950 5700
NoConn ~ 7950 5600
NoConn ~ 7950 5500
$Comp
L telephone-rescue:GND #PWR?
U 1 1 596E8C4F
P 7800 6200
F 0 "#PWR?" H 7800 5950 50  0001 C CNN
F 1 "GND" H 7800 6050 50  0000 C CNN
F 2 "" H 7800 6200 50  0000 C CNN
F 3 "" H 7800 6200 50  0000 C CNN
F 4 "supp" H -600 900 60  0001 C CNN "supplier"
F 5 "supp#" H -600 900 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -600 900 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -600 900 60  0001 C CNN "manf#"
	1    7800 6200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9000 5200 9000 5400
Wire Wire Line
	9000 5400 8900 5400
Wire Wire Line
	9200 6150 9200 6000
Wire Wire Line
	8900 5700 9100 5700
Wire Wire Line
	9100 5800 8900 5800
Wire Wire Line
	8900 5500 9300 5500
Wire Wire Line
	1450 2150 1450 2300
Connection ~ 1450 2300
Connection ~ 1650 7400
Connection ~ 1950 6950
Wire Wire Line
	1950 6950 1950 6850
Wire Wire Line
	1650 2300 1650 2700
Wire Wire Line
	7950 6000 7800 6000
Wire Wire Line
	7800 6000 7800 6200
Wire Wire Line
	1650 7250 1650 7350
Wire Wire Line
	8900 5600 9100 5600
$Comp
L telephone-rescue:GND #PWR?
U 1 1 594E7491
P 4500 1450
F 0 "#PWR?" H 4500 1200 50  0001 C CNN
F 1 "GND" H 4500 1300 50  0000 C CNN
F 2 "" H 4500 1450 50  0000 C CNN
F 3 "" H 4500 1450 50  0000 C CNN
F 4 "supp" H -3850 -1350 60  0001 C CNN "supplier"
F 5 "supp#" H -3850 -1350 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -3850 -1350 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -3850 -1350 60  0001 C CNN "manf#"
	1    4500 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9300 5500 9300 5350
$Comp
L telephone-rescue:CP C3
U 1 1 59AFEEA7
P 2900 7200
F 0 "C3" H 2925 7300 50  0000 L CNN
F 1 "100uF" H 2925 7100 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H5.0mm_P2.50mm" H 2938 7050 50  0001 C CNN
F 3 "" H 2900 7200 50  0001 C CNN
F 4 "supp" H 250 100 60  0001 C CNN "supplier"
F 5 "supp#" H 250 100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 250 100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 250 100 60  0001 C CNN "manf#"
	1    2900 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6400 2900 6500
Wire Wire Line
	3400 6950 3400 7050
Wire Wire Line
	3400 7350 3400 7400
Wire Wire Line
	3400 7400 3300 7400
Wire Wire Line
	2900 7400 2900 7350
Connection ~ 2900 7400
$Comp
L telephone-rescue:CONN_01X04 J4
U 1 1 59FB1329
P 11000 5800
F 0 "J4" H 11000 6050 50  0000 C CNN
F 1 "AMPLIFIER" V 11100 5800 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 11000 5800 50  0001 C CNN
F 3 "" H 11000 5800 50  0001 C CNN
	1    11000 5800
	1    0    0    1   
$EndComp
Text GLabel 8550 4950 1    55   BiDi ~ 0
AUDIO
$Comp
L telephone-rescue:Mounting_Hole MK1
U 1 1 5A610D67
P 5400 7550
F 0 "MK1" H 5400 7750 50  0000 C CNN
F 1 "Hole" H 5400 7675 50  0000 C CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 5400 7550 50  0001 C CNN
F 3 "" H 5400 7550 50  0001 C CNN
	1    5400 7550
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:Mounting_Hole MK2
U 1 1 5A61106B
P 5650 7550
F 0 "MK2" H 5650 7750 50  0000 C CNN
F 1 "Hole" H 5650 7675 50  0000 C CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 5650 7550 50  0001 C CNN
F 3 "" H 5650 7550 50  0001 C CNN
	1    5650 7550
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:C_Small C4
U 1 1 5B38D08D
P 9000 5550
F 0 "C4" H 9010 5620 50  0000 L CNN
F 1 "100nF" H 9010 5470 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 9000 5550 50  0001 C CNN
F 3 "" H 9000 5550 50  0000 C CNN
F 4 "supp" H 7550 3500 60  0001 C CNN "supplier"
F 5 "supp#" H 7550 3500 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 7550 3500 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 7550 3500 60  0001 C CNN "manf#"
	1    9000 5550
	1    0    0    -1  
$EndComp
Connection ~ 9000 5400
Wire Wire Line
	9000 5650 9000 6000
Connection ~ 9000 6000
NoConn ~ 10850 5350
Wire Wire Line
	1650 7250 1950 7250
Wire Wire Line
	1450 2300 1650 2300
Wire Wire Line
	1450 2300 1450 2600
Wire Wire Line
	2900 6950 3400 6950
Wire Wire Line
	2900 6950 2900 7050
Wire Wire Line
	1650 7400 1650 7450
Wire Wire Line
	1950 6950 2050 6950
Wire Wire Line
	9200 6000 9000 6000
Wire Wire Line
	2900 7400 3000 7400
Wire Wire Line
	9000 5400 9000 5450
Wire Wire Line
	9000 6000 8900 6000
$Comp
L telephone-rescue:R-RESCUE-torchy R6
U 1 1 5B57C630
P 6900 1200
F 0 "R6" V 6980 1200 50  0000 C CNN
F 1 "4K7" V 6900 1200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6830 1200 50  0001 C CNN
F 3 "" H 6900 1200 50  0000 C CNN
F 4 "supp" H 1350 -3350 60  0001 C CNN "supplier"
F 5 "supp#" H 1350 -3350 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1350 -3350 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1350 -3350 60  0001 C CNN "manf#"
	1    6900 1200
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R10
U 1 1 5B57C80F
P 7050 1200
F 0 "R10" V 7130 1200 50  0000 C CNN
F 1 "4K7" V 7050 1200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6980 1200 50  0001 C CNN
F 3 "" H 7050 1200 50  0000 C CNN
F 4 "supp" H 1500 -3350 60  0001 C CNN "supplier"
F 5 "supp#" H 1500 -3350 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1500 -3350 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1500 -3350 60  0001 C CNN "manf#"
	1    7050 1200
	1    0    0    1   
$EndComp
$Comp
L Regulator_Linear:L7805 U3
U 1 1 5B5DA2AC
P 2450 6500
F 0 "U3" H 2300 6625 50  0000 C CNN
F 1 "L7805" H 2450 6625 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2475 6350 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2450 6450 50  0001 C CNN
	1    2450 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6950 2050 6500
Wire Wire Line
	2050 6500 2150 6500
Wire Wire Line
	2750 6500 2900 6500
Connection ~ 2900 6500
Wire Wire Line
	2900 6500 2900 6950
Wire Wire Line
	2450 6800 2450 7400
Text GLabel 6850 1700 0    45   Input ~ 0
I2C_DATA
Text GLabel 2200 4100 0    45   Input ~ 0
I2C_DATA
Text GLabel 6850 1600 0    45   Input ~ 0
I2C_CLK
Text GLabel 2200 4000 0    45   Input ~ 0
I2C_CLK
Wire Wire Line
	6850 1600 6900 1600
Wire Wire Line
	7050 1700 7000 1700
Wire Wire Line
	1650 7350 1050 7350
Wire Wire Line
	1650 7400 2450 7400
$Comp
L Device:LED D1
U 1 1 5DEFA9BF
P 3550 3850
F 0 "D1" H 3550 3950 50  0000 C CNN
F 1 "LED" H 3550 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3550 3850 50  0001 C CNN
F 3 "~" H 3550 3850 50  0001 C CNN
	1    3550 3850
	0    -1   -1   0   
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 5DEFC78F
P 10650 1500
F 0 "#PWR?" H 10650 1250 50  0001 C CNN
F 1 "GND" H 10650 1350 50  0000 C CNN
F 2 "" H 10650 1500 50  0000 C CNN
F 3 "" H 10650 1500 50  0000 C CNN
F 4 "supp" H 2300 -1300 60  0001 C CNN "supplier"
F 5 "supp#" H 2300 -1300 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 2300 -1300 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 2300 -1300 60  0001 C CNN "manf#"
	1    10650 1500
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 5DEFD821
P 2900 5250
F 0 "#PWR?" H 2900 5000 50  0001 C CNN
F 1 "GND" H 2900 5100 50  0000 C CNN
F 2 "" H 2900 5250 50  0000 C CNN
F 3 "" H 2900 5250 50  0000 C CNN
F 4 "supp" H -5450 2450 60  0001 C CNN "supplier"
F 5 "supp#" H -5450 2450 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -5450 2450 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -5450 2450 60  0001 C CNN "manf#"
	1    2900 5250
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 5DEFDE69
P 7450 900
F 0 "#PWR?" H 7450 750 50  0001 C CNN
F 1 "+5V" H 7450 1040 50  0000 C CNN
F 2 "" H 7450 900 50  0000 C CNN
F 3 "" H 7450 900 50  0000 C CNN
F 4 "supp" H -900 -600 60  0001 C CNN "supplier"
F 5 "supp#" H -900 -600 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -900 -600 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -900 -600 60  0001 C CNN "manf#"
	1    7450 900 
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 5DEFE527
P 2800 3600
F 0 "#PWR?" H 2800 3450 50  0001 C CNN
F 1 "+5V" H 2800 3740 50  0000 C CNN
F 2 "" H 2800 3600 50  0000 C CNN
F 3 "" H 2800 3600 50  0000 C CNN
F 4 "supp" H -5550 2100 60  0001 C CNN "supplier"
F 5 "supp#" H -5550 2100 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -5550 2100 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -5550 2100 60  0001 C CNN "manf#"
	1    2800 3600
	-1   0    0    -1  
$EndComp
$Comp
L Device:R 10K1
U 1 1 5DEFFDC9
P 8300 1600
F 0 "10K1" V 8380 1600 50  0000 C CNN
F 1 "10K" V 8300 1600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 1600 50  0001 C CNN
F 3 "~" H 8300 1600 50  0001 C CNN
	1    8300 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 900  7450 1000
Wire Wire Line
	7450 1250 7550 1250
Wire Wire Line
	7550 1250 7550 1300
Text GLabel 6800 2400 0    39   Output ~ 0
~PLUG_INT~
$Comp
L Interface_Expansion:PCF8574 U5
U 1 1 5DFCABA3
P 7550 2000
F 0 "U5" H 7200 2600 50  0000 L CNN
F 1 "PCF8574" H 7650 2600 50  0000 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 7550 2000 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCF8574_PCF8574A.pdf" H 7550 2000 50  0001 C CNN
	1    7550 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 1600 8150 1600
Wire Wire Line
	7050 2400 6800 2400
$Comp
L Interface_Expansion:PCF8574 U6
U 1 1 5E00AFCE
P 2900 4400
F 0 "U6" H 2550 5000 50  0000 L CNN
F 1 "PCF8574" H 3000 5000 50  0000 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 2900 4400 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCF8574_PCF8574A.pdf" H 2900 4400 50  0001 C CNN
	1    2900 4400
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 5E06B3E9
P 7650 2800
F 0 "#PWR?" H 7650 2550 50  0001 C CNN
F 1 "GND" H 7650 2650 50  0000 C CNN
F 2 "" H 7650 2800 50  0000 C CNN
F 3 "" H 7650 2800 50  0000 C CNN
F 4 "supp" H -700 0   60  0001 C CNN "supplier"
F 5 "supp#" H -700 0   60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -700 0   60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -700 0   60  0001 C CNN "manf#"
	1    7650 2800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2800 3600 2900 3600
Wire Wire Line
	2900 3600 2900 3700
Wire Wire Line
	7550 2700 7650 2700
Wire Wire Line
	7650 2700 7650 2800
Wire Wire Line
	2900 5100 2900 5250
Wire Wire Line
	3400 4000 3550 4000
$Comp
L telephone-rescue:GND #PWR?
U 1 1 5E08C7C7
P 6950 2200
F 0 "#PWR?" H 6950 1950 50  0001 C CNN
F 1 "GND" H 6950 2050 50  0000 C CNN
F 2 "" H 6950 2200 50  0000 C CNN
F 3 "" H 6950 2200 50  0000 C CNN
F 4 "supp" H -1400 -600 60  0001 C CNN "supplier"
F 5 "supp#" H -1400 -600 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1400 -600 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1400 -600 60  0001 C CNN "manf#"
	1    6950 2200
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 5E08CE00
P 2250 4600
F 0 "#PWR?" H 2250 4350 50  0001 C CNN
F 1 "GND" H 2250 4450 50  0000 C CNN
F 2 "" H 2250 4600 50  0000 C CNN
F 3 "" H 2250 4600 50  0000 C CNN
F 4 "supp" H -6100 1800 60  0001 C CNN "supplier"
F 5 "supp#" H -6100 1800 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -6100 1800 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -6100 1800 60  0001 C CNN "manf#"
	1    2250 4600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7050 1900 6950 1900
Wire Wire Line
	6950 1900 6950 2000
Wire Wire Line
	7050 2000 6950 2000
Connection ~ 6950 2000
Wire Wire Line
	6950 2000 6950 2100
Wire Wire Line
	7050 2100 6950 2100
Connection ~ 6950 2100
Wire Wire Line
	6950 2100 6950 2200
Wire Wire Line
	2400 4500 2250 4500
Wire Wire Line
	2250 4500 2250 4600
Wire Wire Line
	2400 4400 2250 4400
Wire Wire Line
	2250 4400 2250 4500
Connection ~ 2250 4500
Wire Wire Line
	2200 4000 2400 4000
Wire Wire Line
	2400 4100 2200 4100
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 5E0C8796
P 2300 4300
F 0 "#PWR?" H 2300 4150 50  0001 C CNN
F 1 "+5V" H 2300 4440 50  0000 C CNN
F 2 "" H 2300 4300 50  0000 C CNN
F 3 "" H 2300 4300 50  0000 C CNN
F 4 "supp" H -6050 2800 60  0001 C CNN "supplier"
F 5 "supp#" H -6050 2800 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -6050 2800 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -6050 2800 60  0001 C CNN "manf#"
	1    2300 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2400 4300 2300 4300
$Comp
L Device:R R11
U 1 1 5DF3C7E4
P 8300 1700
F 0 "R11" V 8380 1700 50  0000 C CNN
F 1 "R" V 8300 1700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 1700 50  0001 C CNN
F 3 "~" H 8300 1700 50  0001 C CNN
	1    8300 1700
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5DF3D196
P 8300 1800
F 0 "R12" V 8380 1800 50  0000 C CNN
F 1 "10K" V 8300 1800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 1800 50  0001 C CNN
F 3 "~" H 8300 1800 50  0001 C CNN
	1    8300 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5DF3D4CE
P 8300 1900
F 0 "R13" V 8380 1900 50  0000 C CNN
F 1 "10K" V 8300 1900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 1900 50  0001 C CNN
F 3 "~" H 8300 1900 50  0001 C CNN
	1    8300 1900
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5DF3D7AE
P 8300 2000
F 0 "R14" V 8380 2000 50  0000 C CNN
F 1 "10K" V 8300 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 2000 50  0001 C CNN
F 3 "~" H 8300 2000 50  0001 C CNN
	1    8300 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 5DF3DA7B
P 8300 2100
F 0 "R15" V 8380 2100 50  0000 C CNN
F 1 "10K" V 8300 2100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 2100 50  0001 C CNN
F 3 "~" H 8300 2100 50  0001 C CNN
	1    8300 2100
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 5DF3DD69
P 8300 2200
F 0 "R16" V 8380 2200 50  0000 C CNN
F 1 "10K" V 8300 2200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 2200 50  0001 C CNN
F 3 "~" H 8300 2200 50  0001 C CNN
	1    8300 2200
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 5DF3E039
P 8300 2300
F 0 "R17" V 8380 2300 50  0000 C CNN
F 1 "10K" V 8300 2300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 2300 50  0001 C CNN
F 3 "~" H 8300 2300 50  0001 C CNN
	1    8300 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 1700 8150 1700
Wire Wire Line
	8050 1800 8150 1800
Wire Wire Line
	8050 1900 8150 1900
Wire Wire Line
	8050 2000 8150 2000
Wire Wire Line
	8150 2100 8050 2100
Wire Wire Line
	8050 2200 8150 2200
Wire Wire Line
	8150 2300 8050 2300
$Comp
L Device:LED D3
U 1 1 5DFE0C1C
P 3700 3850
F 0 "D3" H 3700 3950 50  0000 C CNN
F 1 "LED" H 3700 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3700 3850 50  0001 C CNN
F 3 "~" H 3700 3850 50  0001 C CNN
	1    3700 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D4
U 1 1 5DFE1212
P 3850 3850
F 0 "D4" H 3850 3950 50  0000 C CNN
F 1 "LED" H 3850 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3850 3850 50  0001 C CNN
F 3 "~" H 3850 3850 50  0001 C CNN
	1    3850 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D5
U 1 1 5DFE16B1
P 4000 3850
F 0 "D5" H 4000 3950 50  0000 C CNN
F 1 "LED" H 4000 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4000 3850 50  0001 C CNN
F 3 "~" H 4000 3850 50  0001 C CNN
	1    4000 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 5DFE1BF0
P 4150 3850
F 0 "D6" H 4150 3950 50  0000 C CNN
F 1 "LED" H 4150 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4150 3850 50  0001 C CNN
F 3 "~" H 4150 3850 50  0001 C CNN
	1    4150 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 5DFE20F6
P 4300 3850
F 0 "D7" H 4300 3950 50  0000 C CNN
F 1 "LED" H 4300 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4300 3850 50  0001 C CNN
F 3 "~" H 4300 3850 50  0001 C CNN
	1    4300 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 4100 3700 4100
Wire Wire Line
	3700 4100 3700 4000
Wire Wire Line
	3850 4000 3850 4200
Wire Wire Line
	3850 4200 3400 4200
Wire Wire Line
	3400 4300 4000 4300
Wire Wire Line
	4000 4300 4000 4000
Wire Wire Line
	3400 4400 4150 4400
Wire Wire Line
	4150 4400 4150 4000
Wire Wire Line
	4300 4000 4300 4500
Wire Wire Line
	4300 4500 3400 4500
$Comp
L Device:LED D8
U 1 1 5E0A7AC4
P 4450 3850
F 0 "D8" H 4450 3950 50  0000 C CNN
F 1 "LED" H 4450 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4450 3850 50  0001 C CNN
F 3 "~" H 4450 3850 50  0001 C CNN
	1    4450 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D9
U 1 1 5E0A7F48
P 4600 3850
F 0 "D9" H 4600 3950 50  0000 C CNN
F 1 "LED" H 4600 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4600 3850 50  0001 C CNN
F 3 "~" H 4600 3850 50  0001 C CNN
	1    4600 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7750 4950 7750 5100
Wire Wire Line
	7750 5400 7950 5400
Wire Wire Line
	7750 4950 9300 4950
Wire Wire Line
	9300 4950 9300 5050
Wire Wire Line
	4450 4000 4450 4600
Wire Wire Line
	4450 4600 3400 4600
Wire Wire Line
	3400 4700 4600 4700
Wire Wire Line
	4600 4700 4600 4000
Wire Wire Line
	3700 3550 3700 3700
Wire Wire Line
	3850 3550 3850 3700
Wire Wire Line
	4000 3550 4000 3700
Wire Wire Line
	4150 3550 4150 3700
Wire Wire Line
	4300 3550 4300 3700
Wire Wire Line
	4450 3550 4450 3700
Wire Wire Line
	4600 3550 4600 3700
Wire Wire Line
	3550 3550 3550 3700
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U1
U 1 1 5E319805
P 2350 1950
F 0 "U1" H 1850 2500 50  0000 L BNN
F 1 "ATtiny85-20PU" H 2450 1400 50  0000 L TNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2350 1950 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 2350 1950 50  0001 C CNN
	1    2350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1350 6900 1600
Connection ~ 6900 1600
Wire Wire Line
	6900 1600 7050 1600
Wire Wire Line
	6900 1050 6900 1000
Connection ~ 7450 1000
Wire Wire Line
	7450 1000 7450 1250
Wire Wire Line
	7050 1050 7050 1000
Wire Wire Line
	6900 1000 7050 1000
Connection ~ 7050 1000
Wire Wire Line
	7050 1000 7450 1000
Wire Wire Line
	10700 6000 10700 5950
Wire Wire Line
	10700 5950 10800 5950
Wire Wire Line
	10800 5850 10700 5850
Wire Wire Line
	10700 5850 10700 5950
Connection ~ 10700 5950
$Comp
L telephone-rescue:+12V #PWR?
U 1 1 5E3DC4FF
P 10800 5350
F 0 "#PWR?" H 10800 5200 50  0001 C CNN
F 1 "+12V" H 10800 5490 50  0000 C CNN
F 2 "" H 10800 5350 50  0000 C CNN
F 3 "" H 10800 5350 50  0000 C CNN
F 4 "supp" H 8150 -1500 60  0001 C CNN "supplier"
F 5 "supp#" H 8150 -1500 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 8150 -1500 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 8150 -1500 60  0001 C CNN "manf#"
	1    10800 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 5650 10800 5350
Wire Wire Line
	1150 7150 1150 6950
Text GLabel 3350 1850 2    45   Input ~ 0
I2C_CLK
Text GLabel 3350 1650 2    45   Input ~ 0
I2C_DATA
Text GLabel 3050 1650 2    45   Input ~ 0
MOSI
Text GLabel 3050 1750 2    45   Input ~ 0
MISO
Text GLabel 3050 1850 2    45   Input ~ 0
CLK
Text GLabel 3100 2150 2    28   Input ~ 0
~RESET~
$Comp
L Connector_Generic:Conn_01x07 J3
U 1 1 5E474A3B
P 4400 1050
F 0 "J3" H 4400 1450 50  0000 C CNN
F 1 "PROG" V 4500 1050 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 4400 1050 50  0001 C CNN
F 3 "~" H 4400 1050 50  0001 C CNN
	1    4400 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 1650 4100 1250
Wire Wire Line
	2950 1650 4100 1650
Wire Wire Line
	4200 1750 4200 1250
Wire Wire Line
	4300 1850 4300 1250
Wire Wire Line
	2950 1850 4300 1850
Wire Wire Line
	4400 2150 4400 1250
Wire Wire Line
	2950 2150 3700 2150
Wire Wire Line
	4500 1450 4500 1250
Wire Wire Line
	4700 1250 4700 1450
Wire Wire Line
	4700 1450 5000 1450
Wire Wire Line
	5000 1450 5000 1100
Wire Wire Line
	7050 1350 7000 1350
Wire Wire Line
	7000 1350 7000 1700
Connection ~ 7000 1700
Wire Wire Line
	7000 1700 6850 1700
Wire Wire Line
	2950 1750 4200 1750
$Comp
L Connector:Barrel_Jack J2
U 1 1 5E4ECB5D
P 750 7250
F 0 "J2" H 750 7450 50  0000 C CNN
F 1 "Conn_01x02" H 750 7050 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 750 7250 50  0001 C CNN
F 3 "~" H 750 7250 50  0001 C CNN
	1    750  7250
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 5E5000C1
P 5150 2400
F 0 "#PWR?" H 5150 2150 50  0001 C CNN
F 1 "GND" H 5150 2250 50  0000 C CNN
F 2 "" H 5150 2400 50  0000 C CNN
F 3 "" H 5150 2400 50  0000 C CNN
F 4 "supp" H -3200 -400 60  0001 C CNN "supplier"
F 5 "supp#" H -3200 -400 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -3200 -400 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -3200 -400 60  0001 C CNN "manf#"
	1    5150 2400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5150 2050 5150 2300
Text Notes 5500 2050 0    45   ~ 0
WINDER\nHANDLE
Text GLabel 3100 2050 2    39   Input ~ 0
~PLUG_INT~
Wire Wire Line
	2950 2050 3100 2050
Text GLabel 3750 1750 2    45   BiDi ~ 0
AUDIO
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5E4FE0E5
P 5350 1950
F 0 "J5" H 5350 2050 50  0000 C CNN
F 1 "Conn_01x02" H 5350 1750 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 5350 1950 50  0001 C CNN
F 3 "~" H 5350 1950 50  0001 C CNN
	1    5350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1950 4900 1950
$Comp
L telephone-rescue:R-RESCUE-torchy R2
U 1 1 5E500578
P 4750 1950
F 0 "R2" V 4830 1950 50  0000 C CNN
F 1 "4K7" V 4750 1950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4680 1950 50  0001 C CNN
F 3 "" H 4750 1950 50  0000 C CNN
F 4 "supp" H -800 -2600 60  0001 C CNN "supplier"
F 5 "supp#" H -800 -2600 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -800 -2600 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -800 -2600 60  0001 C CNN "manf#"
	1    4750 1950
	0    -1   1    0   
$EndComp
Connection ~ 9200 6000
$Comp
L Device:R_POT RV2
U 1 1 5E0ED3CE
P 10300 5850
F 0 "RV2" V 10125 5850 50  0000 C CNN
F 1 "R_POT" V 10200 5850 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-03A_1x03_P2.54mm_Vertical" H 10300 5850 50  0001 C CNN
F 3 "~" H 10300 5850 50  0001 C CNN
	1    10300 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 5750 10550 5750
Wire Wire Line
	10550 5750 10550 5850
Wire Wire Line
	10550 5850 10450 5850
Connection ~ 10300 6000
Wire Wire Line
	9400 5700 9600 5700
Wire Wire Line
	9600 5800 9600 5700
Wire Wire Line
	9400 5800 9600 5800
Connection ~ 9600 5700
$Comp
L telephone-rescue:R-RESCUE-torchy R23
U 1 1 5E321272
P 4600 3400
F 0 "R23" V 4680 3400 50  0000 C CNN
F 1 "120R" V 4600 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4530 3400 50  0001 C CNN
F 3 "" H 4600 3400 50  0000 C CNN
F 4 "supp" H -950 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -950 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -950 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -950 -1150 60  0001 C CNN "manf#"
	1    4600 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R22
U 1 1 5E327F1A
P 4450 3400
F 0 "R22" V 4530 3400 50  0000 C CNN
F 1 "120R" V 4450 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4380 3400 50  0001 C CNN
F 3 "" H 4450 3400 50  0000 C CNN
F 4 "supp" H -1100 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -1100 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1100 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1100 -1150 60  0001 C CNN "manf#"
	1    4450 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R21
U 1 1 5E3283A9
P 4300 3400
F 0 "R21" V 4380 3400 50  0000 C CNN
F 1 "120R" V 4300 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4230 3400 50  0001 C CNN
F 3 "" H 4300 3400 50  0000 C CNN
F 4 "supp" H -1250 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -1250 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1250 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1250 -1150 60  0001 C CNN "manf#"
	1    4300 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R20
U 1 1 5E3288B8
P 4150 3400
F 0 "R20" V 4230 3400 50  0000 C CNN
F 1 "120R" V 4150 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4080 3400 50  0001 C CNN
F 3 "" H 4150 3400 50  0000 C CNN
F 4 "supp" H -1400 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -1400 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1400 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1400 -1150 60  0001 C CNN "manf#"
	1    4150 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R19
U 1 1 5E328D3D
P 4000 3400
F 0 "R19" V 4080 3400 50  0000 C CNN
F 1 "120R" V 4000 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3930 3400 50  0001 C CNN
F 3 "" H 4000 3400 50  0000 C CNN
F 4 "supp" H -1550 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -1550 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1550 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1550 -1150 60  0001 C CNN "manf#"
	1    4000 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R18
U 1 1 5E329171
P 3850 3400
F 0 "R18" V 3930 3400 50  0000 C CNN
F 1 "120R" V 3850 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3780 3400 50  0001 C CNN
F 3 "" H 3850 3400 50  0000 C CNN
F 4 "supp" H -1700 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -1700 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1700 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1700 -1150 60  0001 C CNN "manf#"
	1    3850 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R7
U 1 1 5E329649
P 3700 3400
F 0 "R7" V 3780 3400 50  0000 C CNN
F 1 "120R" V 3700 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3630 3400 50  0001 C CNN
F 3 "" H 3700 3400 50  0000 C CNN
F 4 "supp" H -1850 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -1850 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -1850 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -1850 -1150 60  0001 C CNN "manf#"
	1    3700 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:R-RESCUE-torchy R3
U 1 1 5E329B6F
P 3550 3400
F 0 "R3" V 3630 3400 50  0000 C CNN
F 1 "120R" V 3550 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3480 3400 50  0001 C CNN
F 3 "" H 3550 3400 50  0000 C CNN
F 4 "supp" H -2000 -1150 60  0001 C CNN "supplier"
F 5 "supp#" H -2000 -1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -2000 -1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -2000 -1150 60  0001 C CNN "manf#"
	1    3550 3400
	1    0    0    1   
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 5E32A0BC
P 4750 3150
F 0 "#PWR?" H 4750 3000 50  0001 C CNN
F 1 "+5V" H 4750 3290 50  0000 C CNN
F 2 "" H 4750 3150 50  0000 C CNN
F 3 "" H 4750 3150 50  0000 C CNN
F 4 "supp" H -3600 1650 60  0001 C CNN "supplier"
F 5 "supp#" H -3600 1650 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -3600 1650 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -3600 1650 60  0001 C CNN "manf#"
	1    4750 3150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3550 3250 3700 3250
Wire Wire Line
	3700 3250 3850 3250
Connection ~ 3700 3250
Wire Wire Line
	3850 3250 4000 3250
Connection ~ 3850 3250
Wire Wire Line
	4000 3250 4150 3250
Connection ~ 4000 3250
Wire Wire Line
	4150 3250 4300 3250
Connection ~ 4150 3250
Wire Wire Line
	4300 3250 4450 3250
Connection ~ 4300 3250
Wire Wire Line
	4450 3250 4600 3250
Connection ~ 4450 3250
Wire Wire Line
	4600 3250 4750 3250
Wire Wire Line
	4750 3250 4750 3150
Connection ~ 4600 3250
$Comp
L Device:D_Zener D10
U 1 1 5E3D25E1
P 4600 2150
F 0 "D10" H 4600 2250 50  0000 C CNN
F 1 "5V1_Zener" H 4600 2050 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 4600 2150 50  0001 C CNN
F 3 "~" H 4600 2150 50  0001 C CNN
	1    4600 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 2300 4900 2300
Connection ~ 5150 2300
Wire Wire Line
	5150 2300 5150 2400
Wire Wire Line
	4600 2000 4600 1950
Connection ~ 4600 1950
Wire Wire Line
	4600 1950 2950 1950
NoConn ~ 2400 4800
$Comp
L telephone-rescue:C_Small C5
U 1 1 5E548C5E
P 4900 2100
F 0 "C5" H 4910 2170 50  0000 L CNN
F 1 "100nF" H 4910 2020 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4900 2100 50  0001 C CNN
F 3 "" H 4900 2100 50  0000 C CNN
F 4 "supp" H 3450 50  60  0001 C CNN "supplier"
F 5 "supp#" H 3450 50  60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 3450 50  60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 3450 50  60  0001 C CNN "manf#"
	1    4900 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2000 4900 1950
Connection ~ 4900 1950
Wire Wire Line
	4900 2200 4900 2300
Connection ~ 4900 2300
Wire Wire Line
	4900 2300 5150 2300
NoConn ~ 4600 1250
Connection ~ 2900 6950
Wire Wire Line
	1150 6950 1950 6950
Connection ~ 1650 7350
Wire Wire Line
	1650 7350 1650 7400
Wire Wire Line
	1150 7150 1050 7150
Connection ~ 2450 7400
Wire Wire Line
	2450 7400 2900 7400
Wire Wire Line
	1450 1150 1450 1250
Wire Wire Line
	2350 1350 2350 1250
Wire Wire Line
	2350 1250 1450 1250
Connection ~ 1450 1250
Wire Wire Line
	1450 1250 1450 1950
Wire Wire Line
	1650 2700 2350 2700
Wire Wire Line
	2350 2550 2350 2700
Wire Wire Line
	10300 6000 10700 6000
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5E6A4147
P 9150 3150
F 0 "J6" H 9150 3250 50  0000 C CNN
F 1 "LEDs COMMON CATHODE" H 9150 2950 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 9150 3150 50  0001 C CNN
F 3 "~" H 9150 3150 50  0001 C CNN
	1    9150 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 5E6A4916
P 9150 3600
F 0 "J9" H 9150 3700 50  0000 C CNN
F 1 "Conn_01x02" H 9150 3400 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 9150 3600 50  0001 C CNN
F 3 "~" H 9150 3600 50  0001 C CNN
	1    9150 3600
	1    0    0    -1  
$EndComp
$Comp
L telephone-rescue:GND #PWR?
U 1 1 5E6A6352
P 8850 3950
F 0 "#PWR?" H 8850 3700 50  0001 C CNN
F 1 "GND" H 8850 3800 50  0000 C CNN
F 2 "" H 8850 3950 50  0000 C CNN
F 3 "" H 8850 3950 50  0000 C CNN
F 4 "supp" H 500 1150 60  0001 C CNN "supplier"
F 5 "supp#" H 500 1150 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 500 1150 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 500 1150 60  0001 C CNN "manf#"
	1    8850 3950
	-1   0    0    -1  
$EndComp
$Comp
L telephone-rescue:+5V #PWR?
U 1 1 5E6A68E3
P 8850 2900
F 0 "#PWR?" H 8850 2750 50  0001 C CNN
F 1 "+5V" H 8850 3040 50  0000 C CNN
F 2 "" H 8850 2900 50  0000 C CNN
F 3 "" H 8850 2900 50  0000 C CNN
F 4 "supp" H 450 -1450 60  0001 C CNN "supplier"
F 5 "supp#" H 450 -1450 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 450 -1450 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 450 -1450 60  0001 C CNN "manf#"
	1    8850 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8950 3700 8850 3700
Wire Wire Line
	8850 3700 8850 3950
Wire Wire Line
	8950 3600 8850 3600
Wire Wire Line
	8850 3600 8850 3700
Connection ~ 8850 3700
Wire Wire Line
	8950 3150 8850 3150
Wire Wire Line
	8850 3150 8850 2900
Wire Wire Line
	8950 3250 8850 3250
Wire Wire Line
	8850 3250 8850 3150
Connection ~ 8850 3150
Wire Wire Line
	9200 6000 10300 6000
Wire Wire Line
	9600 5700 10300 5700
Wire Wire Line
	8450 1600 8650 1600
Wire Wire Line
	8650 1600 8650 1350
Wire Wire Line
	8450 1700 8750 1700
Wire Wire Line
	8750 1700 8750 1350
Wire Wire Line
	8450 1800 8850 1800
Wire Wire Line
	8850 1800 8850 1350
Wire Wire Line
	8950 1350 8950 1900
Wire Wire Line
	8950 1900 8450 1900
Wire Wire Line
	9250 1350 9250 2000
Wire Wire Line
	9250 2000 8450 2000
Wire Wire Line
	9350 1350 9350 2100
Wire Wire Line
	9350 2100 8450 2100
Wire Wire Line
	9450 1350 9450 2200
Wire Wire Line
	9450 2200 8450 2200
Wire Wire Line
	8450 2300 9550 2300
Wire Wire Line
	9550 2300 9550 1350
Wire Wire Line
	10650 1450 10650 1500
Text Notes 8800 1050 0    45   ~ 0
FROM JACK SOCKETS
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 60A20F20
P 8750 1150
F 0 "J1" V 8750 1350 50  0000 C CNN
F 1 "INPUT_0-3" V 8850 1350 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 8750 1150 50  0001 C CNN
F 3 "~" H 8750 1150 50  0001 C CNN
	1    8750 1150
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 60A219D6
P 9350 1150
F 0 "J7" V 9350 1350 50  0000 C CNN
F 1 "INPUT_4-7" V 9450 750 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 9350 1150 50  0001 C CNN
F 3 "~" H 9350 1150 50  0001 C CNN
	1    9350 1150
	0    -1   -1   0   
$EndComp
$Comp
L telephone-rescue:CP C?
U 1 1 6265BB45
P 3700 2300
F 0 "C?" H 3725 2400 50  0000 L CNN
F 1 "47uF" H 3725 2200 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H5.0mm_P2.50mm" H 3738 2150 50  0001 C CNN
F 3 "" H 3700 2300 50  0001 C CNN
F 4 "supp" H 1050 -4800 60  0001 C CNN "supplier"
F 5 "supp#" H 1050 -4800 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H 1050 -4800 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H 1050 -4800 60  0001 C CNN "manf#"
	1    3700 2300
	1    0    0    -1  
$EndComp
Connection ~ 3700 2150
Wire Wire Line
	3700 2150 4400 2150
$Comp
L telephone-rescue:GND #PWR?
U 1 1 6265C7E9
P 3700 2450
F 0 "#PWR?" H 3700 2200 50  0001 C CNN
F 1 "GND" H 3700 2300 50  0000 C CNN
F 2 "" H 3700 2450 50  0000 C CNN
F 3 "" H 3700 2450 50  0000 C CNN
F 4 "supp" H -4650 -350 60  0001 C CNN "supplier"
F 5 "supp#" H -4650 -350 60  0001 C CNN "supplier#"
F 6 "1:£; 10:£; 25:£; 100:£" H -4650 -350 60  0001 C CNN "kicost:pricing"
F 7 "manf#" H -4650 -350 60  0001 C CNN "manf#"
	1    3700 2450
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
