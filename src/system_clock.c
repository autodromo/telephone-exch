/*
 * system_clock.c

 * Copyright (c) 2014 Igor Mikolic-Torreira.  All right reserved.
 * Copyright 2017 Mike Evans <mikee@saxicola.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * This is a subset of functions from AVRTools/SystemClock extracted and edited
 * for use with an attiny85
 *
 */


#include "system_clock.h"

volatile unsigned long      timer0_overflow_count = 0;
volatile unsigned long      timer0_millis = 0;
uint8_t                     timer0_fract = 0;
const unsigned long kMicrosecondsPerOverflowTIMER0 = ( clockCyclesToMicroseconds( 64 * 256 ) );
// The whole number of milliseconds per timer0 overflow
const unsigned long kMillisInc = ( clockCyclesToMicroseconds( 64 * 256 ) / 1000 );
// The fractional number of milliseconds per timer0 overflow. Shift right
// by three to fit these numbers into a byte (for 8 MHz and 16 MHz this doesn't lose precision).
const uint8_t kFractInc =  ( ( (clockCyclesToMicroseconds( 64 * 256)) % 1000 ) >> 3 );
const uint8_t kFractMax =  ( 1000 >> 3 );

ISR( TIMER0_OVF_vect )
{
    // Copy these to local variables so they can be stored in registers
    // (volatile variables must be read from memory on every access)
    unsigned long m = timer0_millis;
    uint8_t f = timer0_fract;

    m += kMillisInc;
    f += kFractInc;
    if ( f >= kFractMax )
    {
        f -= kFractMax;
        ++m;
    }

    timer0_fract = f;
    timer0_millis = m;
    timer0_overflow_count++;
}

void initSystemClock()
{
    ATOMIC_BLOCK( ATOMIC_RESTORESTATE )
    {
        // Use Timer0 for the system clock, but configure it so it also supports
        // fast hardware pwm (using phase-correct PWM would mean that Timer0
        // overflowed half as often)

        TCCR0A = 0;     // Clear all settings
        TCCR0B = 0;     // Clear all settings
        TIMSK = 0;     // Disable all interrupts
        TCNT0  = 0;     // initialize counter value to 0

        TCCR0A |= (1 << WGM01) | (1 << WGM00);

        // set timer 0 prescale factor to 64
        TCCR0B |= (1 << CS01) | (1 << CS00);

        // enable timer 0 overflow interrupt
        TIMSK |= (1 << TOIE0);
    }
}

uint32_t millis()
{
    // Disable interrupts while we read timer0_millis or we might get an
    // inconsistent value (e.g. in the middle of a write to timer0_millis)
    unsigned long m;

    ATOMIC_BLOCK( ATOMIC_RESTORESTATE )
    {
        m = timer0_millis;
    }
    return m;
}


uint32_t micros()
{
    // Disable interrupts to avoid reading inconsistent values
    unsigned long m;
    uint8_t t;
    ATOMIC_BLOCK( ATOMIC_RESTORESTATE )
    {
        m = timer0_overflow_count;
        t = TCNT0;

        if ( ( TIFR & _BV(TOV0) ) && ( t < 255 ) )
        {
            m++;
        }
    }
    return ( (m << 8) + t ) * ( 64 / clockCyclesPerMicrosecond() );
}
