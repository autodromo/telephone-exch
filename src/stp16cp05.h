/**
* 16 channels constant current LED driver STP16CP05
*
* Data are sent as an array of uint16_t.  The array can be a single uint16_t.
* \li A bit set to 1 switches on the channel
* \li A bit set to zero switches off the channel.
* \li Data only need be sent when a change in the pattern is required.
*/

#ifndef STP16_H
#define STP16_H
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

volatile uint8_t * stp16_ddr;
volatile uint8_t * stp16_port;
uint8_t stp16_sdiPin; // To STP16CP05
uint8_t stp16_clkPin; // To STP16CP05
uint8_t stp16_lePin;   // To STP16CP05
uint8_t stp16_oePin;  // To STP16CP05


/**
 * Constructor with parameters.
 * To set up AVR the pin modes.
 * @param uint16_t Atmega port for TP16CP05
 * @param uint16_t Atmega DDR for port
 * @param uint8_t Atmega connected sdiPin on STP16CP05
 * @param uint8_t Atmega connected clkPin on STP16CP05
 * @param uint8_t Atmega connected lePin on STP16CP05
 * @param uint8_t Atmega connected oePin on STP16CP05
 */
void stp16_init(volatile uint8_t *port, volatile uint8_t *ddr, uint8_t sdi, uint8_t clk, uint8_t le, uint8_t oe);

void stp16_set_leds(uint16_t* leds, uint8_t len);
void stp16_off(void);
void stp16_on(void);


#endif
