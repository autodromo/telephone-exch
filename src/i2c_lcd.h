/*
 *      atmega8_lcd.h
 *
 *      Copyright 2009 Mike Evans <mikee@saxicola.saxicola.idps.co.uk>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <avr/common.h>


uint8_t lcdInit(void);
void lcdWrite(const char * message, uint8_t line);
void lcdWrite_P(const char * message, uint8_t line);
void lcdWriteChar(uint8_t c);
void lcdwriteBlank(void);
void lcdLightOff(void);
void lcdLightOn(void);
void lcdSetCustom_P(uint8_t position, const char *data);
void lcdSetInstruction(uint8_t);
void lcdSetCursor(uint8_t col, uint8_t row);
