/*
 * pcf8574.c
 *
 * Copyright 2019 Mike Evans <mikee@saxicola.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *  Driver for PCF854
 */


#include "pcf8574.h"

/*
Set all the pins to the appropriate value
*/
uint8_t pcf8574_init()
{
    i2c_init();
}

uint8_t pcf8574_write(uint8_t address, uint8_t value)
{
    uint8_t retval = 0;
    retval = i2c_start((address << 1) | I2C_WRITE);
    i2c_write(value);
    i2c_stop();
    return retval;
}


uint8_t pcf8574_read(uint8_t address)
{
    // Get the pin values.
    uint8_t retval = 0;
    retval = i2c_start((address << 1) | I2C_READ);
    return i2c_readNak();
}
