/*
 * df_player.h
 *
 * Copyright 2017 Mike Evans <mikee@saxicola.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Functions for controlling a df_player mini.
 */


#ifndef DF_PLAYER_H
#define DF_PLAYER_H


//* void(*send_func)() = NULL;
//* HardwareSerial *hserial = NULL;
//* SoftwareSerial *sserial = NULL;
//* boolean is_reply = false;

// 7E FF 06 0F 00 01 01 xx xx EF
// 0    ->  7E is start code
// 1    ->  FF is version
// 2    ->  06 is length
// 3    ->  0F is command
// 4    ->  00 is no receive
// 5~6  ->  01 01 is argument
// 7~8  ->  checksum = 0 - ( FF+06+0F+00+01+01 )
// 9    ->  EF is end code

// Pin definitions used here
#define OUT_A           PB1
#define DDR_PORT        DDRB
#define PIN_PORT        PINB


void mp3_send_cmd (uint8_t cmd);
void send_func (void) ;
void mp3_set_reply (uint8_t state);
void sendBySerial(const uint8_t data) ;
void mp3_fill_cmd_arg (uint8_t cmd, uint16_t arg);
void mp3_fill_cmd (uint8_t cmd);
void play_track(uint8_t track_num, uint8_t volume);
void set_volume(uint8_t volume);


// Get the number of playable files on the SD card.
uint8_t get_file_count();

//
uint16_t mp3_get_checksum (uint8_t *thebuf);

//
void mp3_fill_checksum ();

//
void mp3_play_physical_num (uint16_t num);

// Play a numbered track in the mp3 filder
void mp3_play_in_mp3 (uint16_t num);

//
void mp3_next ();
// Pay nex in specified folder
void mp3_folder_next (uint8_t folder);

//
void mp3_prev ();

//0x06 set volume 0-30
void mp3_set_volume (uint16_t volume);

//0x07 set EQ0/1/2/3/4/5    Normal/Pop/Rock/Jazz/Classic/Bass
void mp3_set_EQ (uint16_t eq);

//0x09 set device 1/2/3/4/5 U/SD/AUX/SLEEP/FLASH
void mp3_set_device (uint16_t device);

void mp3_set_folder (uint16_t folder);

//
void mp3_sleep ();

//
void mp3_reset ();

//
void mp3_pause ();

//
void mp3_stop ();

//
void mp3_play ();

//specify a mp3 file in mp3 folder in your tf card, "mp3_play (1);" mean play "mp3/0001.mp3"
void mp3_play_file (uint16_t num);

// Repeat play files in a folder.
void mp3_repeat_play_folder (uint16_t folder);

void mp3_repeat_play_track (uint16_t track);

void start_repeat(void);
void stop_repeat(void);
//
void mp3_get_state ();

//
void mp3_get_volume ();

//
void mp3_get_u_sum ();

//
void mp3_get_tf_sum ();

//
void mp3_get_flash_sum ();

//
void mp3_get_tf_current ();

//
void mp3_get_u_current ();

//
void mp3_get_flash_current ();

//set single loop
void mp3_single_loop (uint8_t state);

void mp3_single_play (uint16_t num);

//
void mp3_DAC (uint8_t state);

//
void mp3_random_play ();



void initSerial(const uint8_t portPin);
void sendStrBySerial(char *p);
void initSerial(const uint8_t portPin);
void reset_mp3(void);
#endif
