/**********************************************************************
 *      i2c_lcd.c
 *
 *      Copyright 2009 Mike Evans <mikee@saxicola.idps.co.uk>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 **********************************************************************/
/**
* PORT MAPPING
* PCF8574    LCD
* SO16
* P7         DB7
* P6         DB6
* P5         DB5
* P4         DB4
* P2         E
* P0         RS
* P3         LCD LIGHT CONTROL
* P1         R/W
*
*/

/*
Uses these library functions
void _delay_ms(double __ms);
void _delay_us (double __us)

*/

#ifndef I2C_LCD_H
#define I2C_LCD_H
#include "i2c_lcd.h"
#endif

#include <util/delay.h>
#include <avr/pgmspace.h>
#include "i2cmaster.h"
#include <assert.h>

#define LCD_ADDR    0x3F
#define LCD_WAIT    10      // The wait time in the LCD write routine
#define LCD_LWAIT   150     // Long wait time
#define LINES       2       // How many line display to we want?
#define E_HI        0x04
#define RS_HI       0x01
#define RW_HI       0x02
#define BL          0x08    // Backlight

unsigned char buffer = 0;

// Static func defs.
static int write_i2c(unsigned char data);

static void lcdSetLine(uint8_t line);

/*******************************************************************************
Write the buffer contents to the i2c bus
The buffer contains what we would put on the parrallel bus if we were using it.
The PCF8574A board converts this byte to the equivalent parrallel.
*/
static int write_i2c(unsigned char data)
{
    unsigned char retval = 0;
    retval = i2c_start((LCD_ADDR << 1) | I2C_WRITE);
    i2c_write(data);
    i2c_stop();
    return retval;
}


/***********************************************************************
LCD is set to use 4 bit data bus
The LCD is written, and read, via port D

For each data byte we send in two nibbles, MSN,LSN
Data are latched on falling edge of E
See tables 6 and 26 for description.

Wait for power to stabilise before calling this function.

Data are written to the I2C bus into register 0 at the set address.

**********************************************************************/
uint8_t lcdInit(void)
{
    // Check if it's there
    if(i2c_start((LCD_ADDR << 1) | I2C_READ))
    {
        i2c_stop();
        return 1; // 1 denotes no response.
    }
    buffer = 0x00;
    _delay_ms(150);

    // Set up for 4 bit operation
    // These are all 8 bit instructions,
    buffer = 0x30 &~RS_HI | E_HI;
    write_i2c(buffer);
    _delay_us(LCD_WAIT);
    buffer &= ~E_HI;
    write_i2c(buffer);        //buffer |= 0x0c & ~RS_HI & ~E_HI;
    _delay_ms(5);

    buffer = 0x30 & ~RS_HI | E_HI;
    write_i2c(buffer);
    _delay_us(LCD_WAIT);
    buffer &= ~E_HI;
    write_i2c(buffer);
    _delay_ms(5);

    buffer = 0x30 & ~RS_HI | E_HI;
    write_i2c(buffer);
    _delay_ms(LCD_WAIT);
    buffer &= ~E_HI;
    write_i2c(buffer);
    _delay_ms(5);

    buffer = 0x20 & ~RS_HI | E_HI;
    //_delay_us(LCD_WAIT);
    write_i2c(buffer);
    buffer &= ~E_HI;
    write_i2c(buffer);
    _delay_ms(5);

    //Function set 4 bits, 2 lines, 5x8 chars
    // Dl = 0, N = 1, F = 0
    lcdSetInstruction(0x28);

    // Clear display
    lcdSetInstruction(0x08);
    _delay_us(LCD_WAIT);
    _delay_ms(500);

    //Entry mode set, move cursor,
    lcdSetInstruction(0x01);
    _delay_ms(LCD_LWAIT);

    // Home display
    lcdSetInstruction(0x06);
    _delay_ms(LCD_LWAIT);

    //Turn on display and cursor 00001DCB
    // D = Display ON, C = Cursor ON, B = Blinking ON
    lcdSetInstruction(0x0C);
    write_i2c(buffer);
    _delay_ms(LCD_LWAIT);

    lcdLightOn();
    return 0; // Successful init.

}


/***********************************************************************
 *
 *
 */
void lcdSetInstruction(uint8_t c)
{
    // Mask and shift by 4
    uint8_t hiByte = (c & 0b11110000);
    uint8_t loByte = ((c & 0b00001111) << 4);

    buffer = 0x00 | BL;
    write_i2c(buffer);
    _delay_us(LCD_WAIT);

    buffer |= E_HI | hiByte | BL;
    write_i2c(buffer);
    _delay_us(LCD_WAIT);
    buffer &= ~E_HI | BL;
    write_i2c(buffer);
    _delay_us(LCD_WAIT);
    buffer &= ~hiByte | BL;
    write_i2c(buffer);

    buffer |= E_HI | loByte | BL;
    write_i2c(buffer);
    _delay_us(LCD_WAIT);
    buffer &= ~E_HI | BL;
    write_i2c(buffer);
    _delay_us(LCD_WAIT);
    buffer = 0x00 | BL;
    write_i2c(buffer);
    _delay_us(LCD_LWAIT);


}


/***********************************************************************
 * Write directly from program space memory
 * \DONE Write this. my need refactoring of code
 * @param char* Message to be written
 * @param line on which to write
 **********************************************************************/
void
lcdWrite_P(const char *message, uint8_t line)
{
    char i=0;
    // Set the line
    lcdSetLine(line);
    //Write the message loop, until read \0
    while (pgm_read_byte(message))
    {
        lcdWriteChar(pgm_read_byte(message++));
        i++;
    }

    // Blank the rest of the line
    while(i<16){
            lcdWriteChar(0x20);
            i++;
    }
}


/***********************************************************************
 * Write from ordinary RAM
 * @param char* Message to be written
 * @param line on which to write
 **********************************************************************/
void lcdWrite(const char * message, uint8_t line)
{
        uint8_t i = 0;
        // Set the line
        lcdSetLine(line);
//Write the message, loop until read \0
    while  (message[i] != '\0')
    {
        lcdWriteChar(message[i]);
        i++;
    }

// Blank the rest of the line
    while(i<16){
        lcdWriteChar(0x20);
        i++;
    }
}


/**********************************************************************
 * Set which line we are writing to
 * @param char line number 1:2
***********************************************************************/
static void lcdSetLine(uint8_t line)
{
        if (line == 1)
                lcdSetInstruction(0x02);
        if (line == 2)
                lcdSetInstruction(0xC0);
        _delay_ms(LCD_LWAIT);
}


/***********************************************************************
 * Write a single character to the LCD
 * @param char* Character to be written
 * These bits may need to be reversed due to hardware constraints.
***********************************************************************/
void lcdWriteChar(uint8_t c)
{
        uint8_t hiByte = (c & 0b11110000);
        uint8_t loByte = ((c & 0b00001111) << 4);

        _delay_us(LCD_WAIT);
        buffer = RS_HI | E_HI | BL;
        write_i2c(buffer);
        _delay_us(LCD_WAIT);
        buffer |= hiByte;
        write_i2c(buffer);
        _delay_us(LCD_WAIT);
        buffer &= ~E_HI;
        write_i2c(buffer);
        _delay_us(LCD_WAIT);
        buffer &= ~RS_HI & ~hiByte;
        write_i2c(buffer);
        _delay_us(LCD_LWAIT);

        buffer = RS_HI | E_HI | BL;
        write_i2c(buffer);
        _delay_us(LCD_WAIT);
        buffer |= loByte ;
        write_i2c(buffer);
        _delay_us(LCD_WAIT);
        buffer &= ~E_HI;
        write_i2c(buffer);
        _delay_us(LCD_WAIT);
        buffer &= ~RS_HI & ~loByte;
        write_i2c(buffer);
        buffer = BL;
        write_i2c(buffer);


}

/*******************************************************************************
Set the cursor position
@param uint8_t Column
@param uint8_t Row 1 or 2
*/
void  lcdSetCursor(uint8_t col, uint8_t row)
{
    if (row == 2)
    lcdSetInstruction(0x80 + (col + 0x40));
    else lcdSetInstruction(0x80 + (col));
}


/***********************************************************************
Write custom chracter data to the address supplied.
@param uint8_t Character position in the allowed address space. Must be 0 to 7
@param uint8_t[] Array of 8 bytes that form the custom character from PROGMEM array
**********************************************************************/
void lcdSetCustom_P(uint8_t position, const char * data)
{
    assert(position >= 0 && position < 8);
    // Set to write to CGRAM
    lcdSetInstruction(0x40 + (position * 8));
    _delay_ms(LCD_LWAIT);

    for(uint8_t i = 0; i < 8; i++)
    {
        lcdWriteChar(pgm_read_byte(data + i));
    }
    lcdSetInstruction(0x80);
}
/***********************************************************************
Switch the backlight on via port D7
**********************************************************************/
void lcdLightOn()
{
    // Backlight ON
    buffer = BL;
    write_i2c(buffer);
}

/***********************************************************************
Switch the backlight off via port A7
**********************************************************************/
void lcdLightOff()
{
    buffer &= ~BL;
    write_i2c(buffer);
}

