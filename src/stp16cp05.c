/*
* (c) Mike Evans 2015 <mikee@saxicola.co.uk>
* 
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
* 
*/

#include "stp16cp05.h"
#include <util/delay.h>



/**
 * Constructor with parameters.
 * To set up AVR the pin modes.
 * Example:
 * stp16_init(PORTB, DDRB, PB3, PB2, PB0, PB1);
 * @param uint8_t port for TP16CP05
 * @param uint8_t DDR for port
 * @param uint8_t sdiPin on STP16CP05
 * @param uint8_t clkPin on STP16CP05
 * @param uint8_t lePin on STP16CP05
 * @param uint8_t oePin on STP16CP05
 */
void stp16_init(volatile uint8_t * port, volatile uint8_t * ddr, uint8_t sdi, uint8_t clk, uint8_t le, uint8_t oe)    
{
    // Define the pins
    stp16_sdiPin = sdi;
    stp16_clkPin = clk;
    stp16_lePin = le;
    stp16_oePin = oe;
    stp16_ddr = ddr;
    stp16_port = port;
    // Set pins to output.
    *stp16_ddr |= (1 << sdi) | (1 << clk)| (1 << le)| (1 << oe);
    // Set to default state
    *stp16_port |= (1 << stp16_oePin); // HIGH = Disable
    *stp16_port &= ~(1 << stp16_clkPin); // LOW
    *stp16_port &= ~(1 << stp16_lePin); // LOW
    *stp16_port &= ~(1 << stp16_sdiPin); // LOW
}

/**
 * Set the outputs.
 * @param leds array of uint_16.
 * @param len: Length of the data in uint16_t bit blocks.  One block for each chip/block.
 */
void stp16_set_leds(uint16_t *leds, uint8_t len)
{
    int8_t i;
    uint8_t b;
    *stp16_port &= ~(1 << stp16_oePin); //LOW EnABLE
    *stp16_port &= ~(1 << stp16_clkPin); // LOW
    _delay_us(10); // Tsetup_D //
    for(b = 0; b < len; b++)
    {
        for( i = 15; i >= 0; i--)
        {
            //digitalWrite(this->stp16_sdiPin, (leds[b] >> i) & 1); // data out - SDI
            *stp16_port |= (((leds[b] >> i) & 1) << stp16_sdiPin);
            _delay_us(10); // Tsetup_D
            *stp16_port |= (1 << stp16_clkPin); // HIGH
            _delay_us(20); // Twclk + Thold ---> 10+3
            *stp16_port &= ~(1 << stp16_clkPin); // LOW
            *stp16_port &= ~(1 << stp16_sdiPin); // LOW
           
        }
    }
    // Refresh outputs with now data*stp16_port |= (1 << stp16_clkPin); // HIGH
    *stp16_port = (1 << stp16_lePin);// HIGH
    _delay_us(10); // 
    *stp16_port &= ~(1 << stp16_lePin); // LOW
}

/**
 * Turn the chip off
 */
void stp16_off()
{
    *stp16_port |= (1 << stp16_oePin);
    
}
/**
 * Turn the chip on
 */
void stp16_on()
{
    *stp16_port &= ~(1 << stp16_oePin);
}

