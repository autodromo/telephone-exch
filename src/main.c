/***********************************************************************
 *  main.c
 *
 *
 * Copyright 2019 Mike Evans <mikee@saxicola.co.uk>
 * Copyright 2019 Millstream Computing
 *

 TPIC2810DG4

 **********************************************************************/

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/sfr_defs.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <adc.h>
#include <util/atomic.h>
#include "df_player.h"
#include  <avr/eeprom.h>
#include "gitversion.h"
#include "i2cmaster.h"
#include "system_clock.h"
#include "AsmTinySerial.h"
#include <avr/wdt.h>
#include <avr/sfr_defs.h>
#include "pcf8574.h"

#define TRUE 1
#define FALSE 0

//#define TEST // Compile test code sections
#ifdef TEST
#warning Test section active
#endif

// Pin definitions
// See also pin defs for DS3231 i2cmaster.S
#define HANDLE      PB3
#define PLUG_INT    PB4

// Serial expander stuff
#define PLUG_EXP_ADDR   0x38
#define LED_EXP_ADDR    0x39

// Other defines
#define TIMEOUT         20 // Time in milliseconds when we decide the handle is not wound fast enough.
#define PULSE_COUNT     100 // Number of pulses to count
#define MAX_TRACKS      6   // Or max number of call lights.  Should be the same?
#define MAX_LINES       6

// What are we doing?  Define states for our machine
volatile enum states{STARTING, WAITING, PLUGGED, PLAYING} state = STARTING;


// Static function definitions
static void do_handle_activity(void);
static void do_plug_activity(void);
static void test_flash(uint8_t led);
static uint8_t findPosition(unsigned n);
static uint8_t isPowerOfTwo(uint8_t n);
static uint8_t is_value_in_array(uint8_t val, uint8_t arr[]);


// This function is called upon a HARDWARE RESET:
void wdt_first(void) __attribute__((naked)) __attribute__((section(".init3")));

// Clear SREG_I on hardware reset.
void wdt_first(void)
{
    MCUSR = 0; // clear reset flags
    wdt_disable();
    // http://www.atmel.com/webdoc/AVRLibcReferenceManual/FAQ_1faq_softreset.html
}



/***********************************************************************
We need to do long delays in a loop because _delay_ms() longer than the
watchdog will cause a reset.
Take care to set the WDT to more than 10ms
To be accurate I'd need to subtract the call and return overhead. TODO
**********************************************************************/
static void delay_ds(uint16_t deci_seconds)
{
    while(deci_seconds)
    {
        wdt_reset();
        _delay_ms(10);
        deci_seconds --;
    }
}



/***********************************************************************
 * ISR to detect speed of the handle. PCINT3 and interrupts from the
 plug board.
 If it is fast enough, then after a small delay, play the audio.
 *
 **********************************************************************/
ISR(PCINT0_vect)
{
    if(bit_is_set(PINB, PB3)) do_handle_activity();
}


/***********************************************************************
Test if a value is in an array.  If it is return 1, else return 0.
 **********************************************************************/
/*uint8_t is_value_in_array(uint8_t val, uint8_t arr[])
{
    int i;
    for(i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
    {
        if(arr[i] == val)
            return 1;
    }
    return 0;
}*/


/***********************************************************************
 * Do this when the handle is wound.
 * This will be detected using a stepper motor, only do
 * anything if the pulses are fast enough indicating vigorous winding.
 **********************************************************************/
static void do_handle_activity()
{
    static uint32_t time = 0;
    static uint32_t count = 0;
    static uint8_t track = 0;
    uint8_t plug;
    uint8_t leds; // Read into this for current state of the LEDs

    // If we are not waiting for the handle to wound simply return
    if(state != PLUGGED)
    {
        count = 0;
    }
    // It's out first time, just set the time and count +1
    else if(count == 0)
    {
        time = millis();
        count ++;
    }

    else if(count > 0)
    {

        if ((millis() - time) <= TIMEOUT)
        {
            time = millis();
            count ++;
        }
        else count = 0;
    }

    if (count >= PULSE_COUNT) // Play a recording after a small wait.
    {
        state = PLAYING;
    }
}


/***********************************************************************
Generates random numbers in range [lower, upper -1].
Don't return a value that's been recently returned.
Yes I know I should use loops but there are only three checks so...
If it gets longer then :
    temp = a[0];
    for(i = 0; i < N - 1; i++)
        a[i] = a[i + 1];
    a[N - 1] = temp;

@param uint8_t lower
@param uint8_t upper
************************************************************************/
static  uint8_t make_random(uint8_t lower, uint8_t upper)
{
    static uint8_t past_rand[4];
    past_rand[3] = past_rand[2];
    past_rand[2] = past_rand[1];
    past_rand[1] = past_rand[0];
    past_rand[0] =  (rand() % (upper - lower)) + lower;
    if((past_rand[0] == past_rand[1])) make_random(lower, upper);
    else if((past_rand[0] == past_rand[2])) make_random(lower, upper);
    else if((past_rand[0] == past_rand[3])) make_random(lower, upper);
    else return past_rand[0];
}


/***********************************************************************
 * A utility function to check whether n is a power of 2 or not.
 **********************************************************************/
static uint8_t isPowerOfTwo(uint8_t n)
{
    return (n != 0 ) && ((n & (n - 1)) == 0);
}


/***********************************************************************
 * Returns position of the only set bit in 'n'
 * If more than one bit is set, then it's not a power of two so return
 * 255 to signify an error.
 * @return uint8_t bit position or an error code
 **********************************************************************/
static uint8_t findPosition(unsigned n)
{
    if(n == 0) return 0;
    if (!isPowerOfTwo(n))
        return 255;

    unsigned i = 1, pos = 1;

    // Iterate through bits of n until we find a set bit
    // i&n will be non-zero only when 'i' and 'n' have a set bit
    // at same position
    while (!(i & n)) {
        // Unset current bit and set the next bit in 'i'
        i = i << 1;

        // increment position
        ++pos;
    }
    return pos;
}


/**********************************************************************
Cycle through LEDs
**********************************************************************/
static void led_test()
{
    uint8_t l;
    while(pcf8574_read(PLUG_EXP_ADDR) != 0xFF)
    {
        /*for(l = 0; l < 8; l++)
        {
            pcf8574_write(LED_EXP_ADDR, ~_BV(l));
            _delay_ms(100);
        }*/
        l = make_random(0,8);
        pcf8574_write(LED_EXP_ADDR, ~_BV(l));
        _delay_ms(100);
        wdt_reset ();
    }
    pcf8574_write(LED_EXP_ADDR, 0xFF);
}


/***********************************************************************
 * MAIN YAAYYYY
 *
 **********************************************************************/
int main (void)
{
    uint16_t n;
    uint8_t plug_bits; // Bit map of plug position
    uint8_t plug ; // Decimal value of bit position
    uint8_t call_line;
    uint8_t file_count;
    uint8_t track ;
    uint8_t last_track[3];
    //wdt_first(); // Temporarily disable the dog.
    //wdt_disable();
    wdt_enable(WDTO_2S);
    _delay_ms(2000); // Wait for power to stabilise ??
    // Pull up the interrupt sense line.
    PORTB |= _BV(PLUG_INT);

    // Use the internal chip temperature as seed for rand
    ADMUX = 0b00001111 | _BV(REFS1);
    ADCSRA |= (1 << ADEN); // Make sure it's enabled after a reset.
    ADCSRA |= (1 << ADSC); // Start conversion
    while(!(ADCSRA & 0x10)); // wait for conversion done, ADIF flag active
    srand(ADCL);
    // Disable ADC we don't need it any more
    ADCSRA &= ~_BV(ADEN);

    pcf8574_write(LED_EXP_ADDR, 0xFF); // All LEDs OFF

    // Setup the serial output for the SD card player
    initSerial(PB1);
    sei();

    initSystemClock();
    reset_mp3();

    i2c_init();

    // Make sure all the plug lines are pulled up
    pcf8574_write(PLUG_EXP_ADDR, 0xff); // Pulled up

    // Run a simple LED test on boot up.  If it fails the watchdog will reboot.
    wdt_enable(WDTO_2S);

    pcf8574_write(LED_EXP_ADDR, 0x00); // ON
    //while(pcf8574_read(LED_EXP_ADDR) != 0x00);  // Test? Is this pointless?
    _delay_ms(1000);
    wdt_reset ();
    pcf8574_write(LED_EXP_ADDR, 0xFF); // OFF
    _delay_ms(1000); // Make the user think it's thinking
    wdt_reset ();

    if(pcf8574_read(PLUG_EXP_ADDR) != 0xFF)
        led_test();

    wdt_reset ();
    state = STARTING;
    // Now for the real main loop
    //wdt_disable();
    while(1)
    {
        wdt_reset ();
        switch (state){
            /* Select a random destination and illuminate the LED */
            case STARTING:
                while(pcf8574_read(PLUG_EXP_ADDR) != 0xFF)// Wait for any plugs to be removed.
                {
                    wdt_reset (); // Wake the dog
                }
                // Select a random number to light up the display
                call_line = make_random(0, MAX_LINES);
                _delay_ms(1000); // Make the user think it's thinking
                // Light up the display via the serial expander.
                pcf8574_write(LED_EXP_ADDR, ~_BV(call_line));
                state = WAITING;
                break;

            case WAITING: // Light is lit and waiting for a plug.
                // Read the plugboard expander until the user plugs in a jack
                // and check that only one plug is inserted
                // There may be a timeout required to reset the display.
                plug_bits = pcf8574_read(PLUG_EXP_ADDR); // Raw port data
                //if(plug == 0xFF) continue; // No plug inserted.
                plug = findPosition(~plug_bits);
                // Check if it's that same as call_line and if not, try again.
                if(plug == call_line + 1 )
                {
                    state = PLUGGED;
                    // Set up pin interrupts for the handle pulses.
                    PCMSK = _BV(PCINT3);
                    GIMSK |= _BV(PCIE);
                }
                break;

            case PLUGGED: // Player has plugged in a plug, test
                plug_bits = pcf8574_read(PLUG_EXP_ADDR); // Raw port data
                if(plug_bits == 0xFF) // User has unplugged, wait again.
                {
                    GIMSK &= ~_BV(PCIE);
                    state = WAITING;
                    break;
                }
                //if(plug == 255) // The user has plugged in more than one jack.
                if(!isPowerOfTwo(~plug_bits))
                {
                    // Wait for the user to unplug ALL the jacks
                    while(pcf8574_read(PLUG_EXP_ADDR) != 0xFF)
                    {
                        // Flash the entire board to signify an error
                        pcf8574_write(LED_EXP_ADDR, 0x00);
                        _delay_ms(500);
                        pcf8574_write(LED_EXP_ADDR, 0xFF);
                        _delay_ms(500);
                        wdt_reset ();
                    }
                    GIMSK &= ~_BV(PCIE);
                    state = STARTING;
                    break;
                }
                plug = findPosition(~plug_bits);
                if(plug != call_line + 1 )
                {
                    GIMSK &= ~_BV(PCIE);
                    state = WAITING;
                }
                #ifdef TEST
                // Don't wait for the handle to be wound
                #warning Test section active
                state  = PLAYING;
                #endif
                break;

            // Play the track and wait for it to finish
            case PLAYING: // PLAYING state gets set in ISR
                // Disable pin change interrupts
                GIMSK = 0;
                // Get plugged plug
                plug = pcf8574_read(PLUG_EXP_ADDR);
                #ifdef TEST
                #warning Test section active
                    track = call_line;
                #else
                    track = findPosition(~plug);
                #endif
                // Play track
                sei ();
                play_track (track, 30);
                // Wait for player to finish
                _delay_ms(100);
                DDRB &= ~(1 << OUT_A);  // Make pin input

                // Wait until the triggered play is done
                while (!(PINB & (1 << OUT_A)))
                {
                    wdt_reset ();
                    if(pcf8574_read(PLUG_EXP_ADDR) == 0xFF) // User has pulled the plug
                    {
                        reset_mp3();
                        //break;
                    }
                    if(!isPowerOfTwo(~pcf8574_read(PLUG_EXP_ADDR)))
                    {
                        reset_mp3();
                        //break;
                    }
                }
                _delay_ms(1000);
                wdt_reset ();
                // Switch off the call lights
                pcf8574_write(LED_EXP_ADDR, 0xff);
                // Wait for the plug to be removed.
                while(pcf8574_read(PLUG_EXP_ADDR) != 0xFF)
                {
                    wdt_reset ();
                }
                sei ();
                state = STARTING;
                break;
        }

    }
    return 0; // Should never reach here. :)
}
